<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::group(['prefix' => '/admin/', 'namespace' => 'Admin',
    'middleware' => ['auth', 'role:Admin|Operator']
], function () {
    Route::get('/dashboard', function () {
        return view('admin.dashboard.index');
    })->name('dashboard');

    Route::group(['prefix' => '/access/', 'namespace' => 'Access'], function () {
        Route::resources([
            'user' => 'UserController',
            'role' => 'RoleController'
        ]);
    });


    Route::resources([
        'bus' => 'BusController',
        'bus-type' => 'BusTypeController',
        'route' => 'RouteController',
        'city' => 'CityController',
        'gallery' => 'GalleryController',
        'amenities' => 'AmenitiesController',
        'seat-arrangement' => 'SeatArrangementController',
        'booking' => 'BookingController',
        'operator' => 'OperatorController'
    ]);
    Route::get('all-shifts', "BusController@getAllShifts")->name('route-list');
    Route::get('route-list', "BusController@routeList")->name('route-list');
    Route::get('bus_type', "BusController@busType")->name('bus_type');
    Route::get('seat-arrangement/{id}/duplicate', "SeatArrangementController@duplicate")->name('seat-arrangement.duplicate');
    Route::get('seat-arrangement/assign/{id}', "SeatArrangementController@assign");
    Route::post('seat-arrangement/assign', "SeatArrangementController@postAssign")->name('seat-arrangement.assign');
    Route::get('bus-type/toggle/{id}', "BusTypeController@toggleStatus")->name('bus-type.toggle');
    Route::get('stop-delete', "BusController@stopDelete")->name('stop-delete');
    Route::get('amenities/toggle/{id}', "AmenitiesController@toggleStatus")->name('amenities.toggle');
    Route::get('bus/toggle/{id}', "BusController@toggleStatus")->name('bus.toggle');
    Route::get('bus/toggle-approval/{id}', "BusController@toggleApproval")->name('bus.toggle-approval');
    Route::get('operators/toggle/{id}', "OperatorController@toggleStatus")->name('operators.toggle');
    Route::get('operators/toggleapproval/{id}', "OperatorController@toggleApproval")->name('operators.toggleapproval');
    Route::get('city/toggle/{id}', "CityController@toggleStatus")->name('city.toggle');
    Route::get('route/toggle/{id}', "RouteController@toggleStatus")->name('route.toggle');

});
Route::get('partner', "PagesController@partner")->name('partner-home');

Route::get('operator/register', "Auth\OperatorRegisterController@showRegistrationForm")->name('auth.operator.register');
Route::post('operator/register', "Auth\OperatorRegisterController@register");

//Route::get('/', 'HomeController@index')->name('home');
//Route::get('/search', 'SearchController@index')->name('search.index');
Auth::routes();
