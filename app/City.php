<?php

namespace App;

use App\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name'];

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::addGlobalScope(new StatusScope);
//    }

    public function routes()
    {
        return $this->belongsToMany(Route::class)->withPivot('position');
    }
    public function stops()
    {
        return $this->hasMany(Stop::class);
    }
}
