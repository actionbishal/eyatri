<?php

namespace App;

use App\Http\Traits\EnumTrait;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use EnumTrait;

    protected $fillable = [
        'name', 'contact_no', 'registration_number', 'route_id', 'type_id', 'operator_id', 'seat_arrangement_id'
    ];

// TODO fix migration Dec/12/2018

    public function busType()
    {
        return $this->belongsTo(BusType::class, 'type_id');
    }

    public function route()
    {
        return $this->hasOne(Route::class);
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function amenities()
    {
        return $this->belongsToMany(Amenities::class, 'amenities_buses');
    }

    public function schedules()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function seat_arrangement()
    {
        return $this->belongsToMany(SeatArrangement::class);
    }

    public function stops()
    {
        return $this->hasMany(Stop::class);
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

}
