<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $fillable = [
        'bus_type_id','name','isCabin','status','seat_arrangement_id'
    ];
    protected $casts = [
        'isCabin' => 'boolean',
        'status' => 'boolean'
    ];
    public function seat_arrangement()
    {
        return $this->belongsTo(SeatArrangement::class);
    }
}
