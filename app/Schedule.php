<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'bus_id','sunday' , 'monday', 'tuesday', 'wednesday', 'thursday', 'friday','saturday'
    ];
    public function buses()
    {
        return $this->belongsTo(Bus::class);
    }
}
