<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stop extends Model
{
    protected $fillable = [
        'name','description','time','city_id','bus_id'
    ];

    public function cities()
    {
        return $this->belongsTo(City::class);
    }
    public function buses()
    {
        return $this->belongsTo(Bus::class);
    }
}
