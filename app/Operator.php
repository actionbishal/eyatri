<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Operator extends Model
{
    protected $fillable = [
        'user_id', 'name','contact_no','address'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bus()
    {
        return $this->hasMany(Bus::class);
    }

    public static function hasBus($id)
    {
        $current_operator_id = Auth::user()->operator()->first()->id;
        return Bus::where('id', $id)->where('operator_id', $current_operator_id)->exists();
    }
}
