<?php

namespace App\Http\Controllers\Admin;

use App\Amenities;
use App\Bus;
use App\BusType;
use App\Route;
use App\SeatArrangement;
use App\Stop;
use App\Operator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->hasRole('Operator')) {
            $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
            $buses = Bus::where('operator_id', $operator_id)->get();
        } else {
            $buses = Bus::all();
        }

        return view('admin.bus.index')->with('buses', $buses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $routes = Route::all();
        $bus_types = BusType::all();
        $amenities = Amenities::all();
        $operators = Operator::all();
        $seat_arrangements = SeatArrangement::all();
        $shifts = Bus::getPossibleEnumValues('shift');

        return view('admin.bus.create')
            ->with('bus_types', $bus_types)
            ->with('routes', $routes)
            ->with('operators', $operators)
            ->with('seat_arrangements', $seat_arrangements)
            ->with('shifts', $shifts)
            ->with('amenities', $amenities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type_id' => 'required',
            'registration_number' => 'required',
            'contact_no' => 'required',
        ]);

        $operator_id = $request->operator_id ?
            $request->operator_id :
            Operator::where('user_id', Auth::user()->id)->first()->id;

        $bus = Bus::create([
            'name' => $request->name,
            'contact_no' => $request->contact_no,
            'operator_id' => $operator_id,
            'registration_number' => $request->registration_number,
            'shift' => $request->shift,
            'type_id' => $request->type_id
        ]);
        $bus->amenities()->attach($request->amenities);

        Session::flash('success', 'Successfully Added!');

        return redirect()->route('bus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->hasRole('Operator') && !Operator::hasBus($id)) {
            return redirect()->route('bus.index');
        }

        $bus = Bus::find($id);
        $bus_amenities = [];

        foreach ($bus->amenities as $bus_amenity) {
            array_push($bus_amenities, $bus_amenity->id);
        }

        if (Auth::user()->hasRole('Operator')) {
            $seat_arrangements = SeatArrangement::whereHas('operator', function ($query) {
                $query->where('operators.id', Operator::where('user_id', Auth::user()->id)->first()->id);
            })->get();
        } else {
            $seat_arrangements = SeatArrangement::all();
        }


        return view('admin.bus.edit', ['bus' => $bus,
            'amenities' => Amenities::all(),
            'routes' => Route::all(),
            'operators' => Operator::all(),
            'bus_amenities' => $bus_amenities,
            'seat_arrangements' => $seat_arrangements,
            'bus_types' => BusType::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd(array_filter($request->schedules));
        $this->validate($request, [
            'name' => 'required',
            'registration_number' => 'required'
        ]);
        $operator_id = $request->operator_id ?
            $request->operator_id :
            Operator::where('user_id', Auth::user()->id)->first()->id;

        Bus::find($id)->update([
            'name' => $request->name,
            'contact_no' => $request->contact_no,
            'operator_id' => $operator_id,
            'registration_number' => $request->registration_number,
            'type_id' => $request->type_id,
        ]);
        Bus::find($id)->amenities()->sync($request->amenities);
        Bus::find($id)->seat_arrangement()->sync($request->seat_arrangement_id);

        if ($request->stop_name) {
            foreach (array_filter($request->stop_name) as $index => $stop_name) {
                Bus::find($id)->stops()->updateOrCreate([
                    'id' => $request->stop[$index]
                ], [
                    'name' => $stop_name,
                    'description' => $request->description[$index],
                    'time' => $this->parseTime($request->time[$index]),
                    'city_id' => $request->city[$index],
                ]);
            }
        }
        Bus::find($id)->schedules()->updateOrCreate([
            'bus_id' => $request->bus_id,
            'sunday' => $request->Sunday,
            'monday' => $request->Monday,
            'tuesday' => $request->Tuesday,
            'wednesday' => $request->Wednesday,
            'thursday' => $request->Thursday,
            'friday' => $request->Friday,
            'saturday' => $request->Saturday,
        ]);


        Session::flash('success', 'Successfully updated!');

        return redirect()->route('bus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bus::destroy($id);
        return response('success');
    }

    public function routeList(Request $request)
    {
        $route = Route::where('id', $request->id)->first();
        $bus = Bus::find($request->bus);
        return view('admin.bus._partials.ajax_route_list')
            ->with('route', $route)
            ->with('bus', $bus);
    }

    public function parseTime($time)
    {
        return Carbon::createFromTimeStamp(strtotime($time));
    }

    public function stopDelete(Request $request)
    {
        Stop::destroy($request->id);
    }

    public function toggleStatus($id)
    {
        $buses = Bus::findOrFail($id);
        Bus::where('id', $id)->update(['status' => !$buses->status]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('bus.index');
    }

    public function toggleApproval($id)
    {
        $bus = Bus::findOrFail($id);
        Bus::where('id', $id)->update(['approval' => !$bus->approval]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('bus.index');
    }

    public function busType(Request $request)
    {
        if (Auth::user()->hasRole('Operator')) {
            $seat_arrangements = SeatArrangement::where('bus_type_id', $request->id)
                ->whereHas('operator', function ($query) {
                    $query->where('operators.id', Operator::where('user_id', Auth::user()->id)->first()->id);
                })
                ->get();
        } else {
            $seat_arrangements = SeatArrangement::where('bus_type_id', $request->id)->get();
        }
        return $seat_arrangements;
    }
}
