<?php

namespace App\Http\Controllers\Admin;

use App\Operator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::all();

        return view('admin.operator.index')->with('operators', $operators);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Operator::where('name', $request->name)->first()) {
            Operator::create([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully Added!');
            Session::flash('alert-class', 'alert-success');
        } else {
            Session::flash('message', 'Sorry!! Looks like Operator with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->route('operator.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operators = Operator::find($id);
        return view('admin.operator.edit', ['operators' => $operators]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Operator::where('name', $request->name)->first()) {
            Operator::where('id', $id)->update([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully updated!');
            Session::flash('alert-class', 'alert-info');
        } else {
            Session::flash('message', 'Sorry!! Looks like Operator with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }
        Operator::where('id', $id)->update([
            'contact_no' => $request->input('contact_no'),
            'address' => $request->input('address')
        ]);


        return redirect()->route('operator.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Operator::destroy($id);
        return response('success');
    }

    public function toggleStatus($id)
    {
        $operators = Operator::findOrFail($id);
        Operator::where('id', $id)->update(['status' => !$operators->status]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('operator.index');
    }

    public function toggleApproval($id)
    {
        $operators = Operator::findOrFail($id);
        Operator::where('id', $id)->update(['approval' => !$operators->approval]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('operator.index');
    }
}
