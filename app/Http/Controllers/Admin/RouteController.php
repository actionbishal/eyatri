<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Operator;
use App\Route;
use App\Bus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->hasRole('Operator')) {
            $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
            $routes = Route::whereHas('bus', function ($query) use ($operator_id) {
                $query->where('operator_id', $operator_id);
            })->get();
        } else {
            $routes = Route::all();
        }
        return view('admin.route.index')->with('routes', $routes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $route = Route::all();
        if (Auth::user()->hasRole('Operator')) {
            $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
            $buses = Bus::where('operator_id', $operator_id)->get();
        } else {
            $buses = Bus::all();
        }
        $cities = City::all();

        return view('admin.route.create')
            ->with('buses', $buses)
            ->with('route', $route)
            ->with('cities', $cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bus_id' => 'sometimes|required|unique:routes,bus_id,' . $request->bus_id,
            'city' => 'required',
            'price' => 'required',
        ]);

        $cities = $request->city;
        $times = $request->time;
        $sync_data = [];
        for ($i = 0; $i < count($cities); $i++) {
            $sync_data[$cities[$i]] = ['time' => $times[$i]];
        }

        Route::create([
            'price' => $request->price,
            'bus_id' => $request->bus_id,
//            'arrival_time' =>$this->parseTime( $request->arrival_time),
        ])->cities()->sync($sync_data);
        Session::flash('message', 'Successfully added');
        Session::flash('alert-class', 'alert-success');

        return redirect()->route('route.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function parseTime($time)
    {
        return Carbon::createFromTimeStamp(strtotime($time));
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $route = Route::find($id);
        if (Auth::user()->hasRole('Operator')) {
            $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
            $buses = Bus::where('operator_id', $operator_id)->get();
        } else {
            $buses = Bus::all();
        }

        $cities = City::all();

        return view('admin.route.edit', [
            'route' => $route,
            'buses' => $buses,
            'cities' => $cities
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'bus_id' => 'required|unique:routes,bus_id,' . $request->bus_id,
            'city' => 'required',
            'price' => 'required',
        ]);

        Route::where('id', $id)->update([
            'bus_id' => $request->bus_id,
            'price' => $request->price,
            'arrival_time' => $this->parseTime($request->arrival_time),
            'departure_time' => $this->parseTime($request->departure_time),
        ]);
        Route::find($id)->cities()->sync(array_filter($request->city));
        Session::flash('message', 'Successfully updated');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('route.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Route::destroy($id);

        return response('success');
    }

    public function toggleStatus($id)
    {
        $routes = Route::findOrFail($id);
        Route::where('id', $id)->update(['status' => !$routes->status]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('route.index');
    }
}
