<?php

namespace App\Http\Controllers\Admin;
use App\Amenities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;



class AmenitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amenities=Amenities::all();
        return view('admin.amenities.index')->with('amenities',$amenities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Amenities::where('name', $request->name)->first()) {
            Amenities::create([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully Added!');
            Session::flash('alert-class', 'alert-success');
        } else {
            Session::flash('message', 'Sorry!! Looks like Amenities with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->route('amenities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $amenities = Amenities::find($id);
        return view('admin.amenities.edit' ,['amenities' => $amenities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Amenities::where('name', $request->name)->first()) {
            Amenities::where('id', $id)->update([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully updated!');
            Session::flash('alert-class', 'alert-info');
        } else {
            Session::flash('message', 'Sorry!! Looks like Amenities with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->route('amenities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Amenities::destroy($id);
        return response('success');
    }
    public function toggleStatus($id)
    {
        $amenities = Amenities::findOrFail($id);
        Amenities::where('id',$id)->update(['status'=>!$amenities->status]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('amenities.index');
    }
}