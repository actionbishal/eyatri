<?php

namespace App\Http\Controllers\Admin;

use App\BusType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BusTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $bus_types = BusType::all();

        return view('admin.bustype.index')->with('bus_types', $bus_types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!BusType::where('name', $request->name)->first()) {
            BusType::create([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully Added!');
            Session::flash('alert-class', 'alert-success');
        } else {
            Session::flash('message', 'Sorry!! Looks like Bus Type with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->route('bus-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bus_type = BusType::find($id);

        return view('admin.bustype.edit', ['bus_type' => $bus_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!BusType::where('name', $request->name)->first()) {
            BusType::where('id', $id)->update([
                'name' => $request->input('name')
            ]);
            Session::flash('message', 'Successfully updated!');
            Session::flash('alert-class', 'alert-info');
        } else {
            Session::flash('message', 'Sorry!! Looks like Bus Type with this name already exists.');
            Session::flash('alert-class', 'alert-danger');
        }
        
        return redirect()->route('bus-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

    public function toggleStatus($id)
    {
        $bus_type = Bustype::findOrFail($id);
        BusType::where('id', $id)->update(['status' => !$bus_type->status]);
        Session::flash('message', 'Successfully updated!');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('bus-type.index');
    }
}
