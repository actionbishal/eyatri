<?php

namespace App\Http\Controllers\Admin;

use App\Bus;
use App\BusType;
use App\Operator;
use App\OperatorSeatArrangement;
use App\Seat;
use App\SeatArrangement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SeatArrangementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SeatArrangement[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $seat_arrangements = SeatArrangement::withCount(['seats as cabin_counts' => function ($query) {
            $query->where('isCabin', 1)->where('status', 1);
        }])->withCount(['seats as back_seats_counts' => function ($query) {
            $query->where('isCabin', 0)->where('status', 1);
        }])->get();

        if (Auth::user()->hasRole('Operator')) {
            $seat_arrangements->map(function ($seat_arrangement) {
                $seat_arrangement['is_operator_seat_arrangement'] = OperatorSeatArrangement::where('operator_id', Operator::where('user_id', Auth::user()->id)->first()->id)
                    ->where('seat_arrangement_id', $seat_arrangement->id)->exists();

                return $seat_arrangement;
            });
        }

        return view('admin.seat_arrangement.index')->with('seat_arrangements', $seat_arrangements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bus_types = BusType::all();
        return view('admin.seat_arrangement.create')->with('bus_types', $bus_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type_id' => 'required',
            'seat_name.*' => 'max:3'
        ]);
        $seat_arrangement = SeatArrangement::create([
            'bus_type_id' => $request->input('type_id'),
            'name' => $request->name,
            'seat_type' => $request->seat_type,
        ]);

        foreach ($request->seat_name as $index => $seat_name) {
            Seat::create([
                'seat_arrangement_id' => $seat_arrangement->id,
                'isCabin' => $request->isCabin[$index],
                'status' => $request->seat_status[$index],
                'name' => $seat_name,
            ]);
        }
        if (Auth::user()->hasRole('Operator')) {
            $seat_arrangement->operator()->attach(Operator::where('user_id', Auth::user()->id)->first()->id);
        }

        return redirect()->route('seat-arrangement.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seat_arrangement = SeatArrangement::find($id);

        return view('admin.seat_arrangement.show')->with('seat_arrangement', $seat_arrangement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bus_types = BusType::all();

        $seat_arrangement = SeatArrangement::withCount(['seats as cabin_counts' => function ($query) {
            $query->where('isCabin', 1)->where('status', 1);
        }])->withCount(['seats as back_seats_counts' => function ($query) {
            $query->where('isCabin', 0)->where('status', 1);
        }])->find($id);


        return view('admin.seat_arrangement.edit', ['seat_arrangement' => $seat_arrangement, 'bus_types' => $bus_types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
//            'type_id' => 'required',
            'seat_name.*' => 'max:3'
        ]);

        SeatArrangement::where('id', $id)->update([
//            'bus_type_id' => $request->input('type_id'),
            'name' => $request->name,
//            'seat_type' => $request->seat_type,
        ]);

        foreach ($request->seat_name as $index => $seat_name) {
            Seat::where('id', $request->seat_id[$index])->update([
                'isCabin' => $request->isCabin[$index],
                'status' => $request->seat_status[$index],
                'name' => $seat_name,
            ]);
        }
        Session::flash('success', 'Successfully updated!');

        return redirect()->route('seat-arrangement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SeatArrangement::destroy($id);
        return response('success');
    }

    public function duplicate($id)
    {
//        SeatArrangement::with('seats')->where('id',$id)->first()->replicate();
        $seat_arrangement = SeatArrangement::with('seats')->where('id', $id)->first();
        $new_seat_arrangement = $seat_arrangement->replicate();
        $new_seat_arrangement->save();

        foreach ($seat_arrangement->seats()->get() as $q) {
            $question = Seat::where('id', $q->id)->first();
            $new_seat = $question->replicate();
            $new_seat->seat_arrangement_id = $new_seat_arrangement->id;
            $new_seat->save();
        }

        if (Auth::user()->hasRole('Operator')) {
            $new_seat_arrangement->operator()->attach(Operator::where('user_id', Auth::user()->id)->first()->id);
        }

//        OperatorSeatArrangement::create([
//            'operator_id' => Operator::where('user_id', Auth::user()->id)->first()->id,
//            'seat_arrangement_id' => $new_seat_arrangement->id
//        ]);
        Session::flash('message', 'Successfully Duplicated! You can modify');
        Session::flash('alert-class', 'alert-info');

        return redirect()->route('seat-arrangement.index');
    }

    public function assign($id)
    {
        $seat_arrangement = SeatArrangement::find($id);

        if (Auth::user()->hasRole('Operator')) {
            $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
            $buses = Bus::where('operator_id', $operator_id)->get();
        } else {
            $buses = Bus::all();
        }
        return view('admin.seat_arrangement.assign')
            ->with('seat_arrangement', $seat_arrangement)
            ->with('buses', $buses);
    }

    public function postAssign(Request $request)
    {
        $request->validate([
            'bus_id' => 'required',
            'seat_arrangement_id' => 'required'
        ]);

        Bus::find($request->bus_id)->seat_arrangement()->sync($request->seat_arrangement_id);

        return redirect()->route('seat-arrangement.index');
    }
}
