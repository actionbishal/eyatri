<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Bus;
use App\City;
use App\Operator;
use App\Route;
use App\SeatArrangement;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $request->validate([
            'source' => 'required',
            'destination' => 'required'
        ]);

        $buses = Bus::with(['route.cities', 'amenities', 'stops', 'busType', 'seat_arrangement.seats','bookings'
        ])->whereHas('route.cities', function ($query) use ($request) {
            $query->where('cities.id', $request->source);
        })->whereHas('route.cities', function ($query) use ($request) {
            $query->where('cities.id', $request->destination);
        })->whereHas('busType', function ($query) use ($request) {
            $query->when($bus_types = $request->filters['bus_types'], function ($q) use ($bus_types, $request) {
                $q->whereIn('bus_types.name', $bus_types);
            });
        })->get();

        $filtered_buses = [];
        $source_position = '';
        $destination_position = '';
        foreach ($buses as $bus) {
            foreach ($bus->route->cities as $city) {
                if ($city->id == $request->source)
                    $source_position = $city->pivot->position;
                if ($city->id == $request->destination)
                    $destination_position = $city->pivot->position;
            }
            if ($source_position < $destination_position) {
                array_push($filtered_buses, $bus);
            }
        }

        return ['buses' => $filtered_buses];
    }

    public function cities()
    {
        $cities = City::all();

        return $cities;
    }

    public function routes()
    {
        $routes = Route::all();

        return $routes;
    }

    public function operators()
    {
        $operators = Operator::all();

        return $operators;
    }

    public function seatArrangement(Request $request)
    {
        $seat_arrangements = SeatArrangement::whereHas('buses', function ($q) use ($request) {
            $q->where('buses.id', $request->bus_id);
        })->with('seats')->first();
//
        return $seat_arrangements;
    }
    public function storeDetails(Request $request)
    {

         Booking::create([
            'fullname' => $request->fname,
            'age' => $request->age,
            'email' => $request->email,
            'contact' => $request->contact,
            'seats' => $request->seats,
            'amount' => $request->total_fare,
            'boarding_point' => $request->selected_boarding_point,
            'bus_id' => $request->bus,
        ]);

    }

}
