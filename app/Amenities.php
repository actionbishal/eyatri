<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    protected $fillable = [
        'name'
    ];
    public function bus()
    {
        return $this->belongsToMany(Bus::class, 'amenities_buses');
    }
}
