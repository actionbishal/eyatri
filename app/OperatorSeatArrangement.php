<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperatorSeatArrangement extends Model
{
    protected $table = 'operator_seat_arrangement';
    protected $fillable = [
        'operator_id', 'seat_arrangement_id'
    ];
}
