<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatArrangement extends Model
{
    protected $fillable = [
        'bus_type_id','name', 'seat_type'
    ];

    public function seats()
    {
        return $this->hasMany(Seat::class);
    }
    public function bus_type()
    {
        return $this->belongsTo(BusType::class);
    }
    public function operator()
    {
        return $this->belongsToMany(Operator::class);
    }
    public function buses()
    {
        return $this->belongsToMany(Bus::class);
    }

}
