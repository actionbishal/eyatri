<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\BelongsToSortedManyTrait;

class Route extends Model
{
    use BelongsToSortedManyTrait;

    protected $fillable = [
        'name', 'status','price','bus_id'
    ];


    public function bus()
    {
        return $this->belongsTo(Bus::class);
    }

    public function cities()
    {
        return $this->belongsToSortedMany(City::class);
    }
}
