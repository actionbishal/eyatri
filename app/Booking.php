<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'fullname', 'email', 'contact', 'age', 'amount', 'status', 'bus_id', 'seats', 'traveldate', 'boarding_point'
    ];
    protected $casts = [
        'seats' => 'array'
    ];
    public function bus()
    {
        return $this->belongsTo(Bus::class);
    }
}
