<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buses', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('contact_no')->nullable();
            $table->string('registration_number');
            $table->integer('operator_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->boolean('approval')->default('0');
            $table->boolean('status')->default('1');
            $table->enum('shift',['Day','Night'])->default('Day');
            $table->foreign('type_id')->references('id')->on('bus_types')->onDelete('cascade');
            $table->foreign('operator_id')->references('id')->on('operators')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buses');
    }
}
