<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('seat_arrangement_id')->unsigned();
            $table->string('name')->nullable();
            $table->boolean('isCabin')->default(0);
            $table->boolean('status')->default(1);
            $table->foreign('seat_arrangement_id')->references('id')->on('seat_arrangements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seats');
    }
}
