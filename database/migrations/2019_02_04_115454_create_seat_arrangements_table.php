<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatArrangementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seat_arrangements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bus_type_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('seat_type');
            $table->foreign('bus_type_id')->references('id')->on('bus_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seat_arrangements');
    }
}
