<?php

use App\Amenities;
use Illuminate\Database\Seeder;

class AmenitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amenities = [
            [
                'name' => "Blanket"

            ],
            [
                'name' => "Wifi"

            ],
            [
                'name' => "Charger"

            ],
            [
                'name' => "Movie"

            ],
            [
                'name' => "Pillow"

            ],
            [
                'name' => "Snacks"

            ],
        ];

        foreach ($amenities as $key => $value) {
            Amenities::create($value);
        }
    }
}
