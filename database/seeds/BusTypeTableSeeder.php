<?php
use App\BusType;
use Illuminate\Database\Seeder;

class BusTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bus_types = [
            [
                'name' => "Night Deluxe",
                'status' => "1"

            ],
            [
                'name' => "Hiace",
                'status' => "1"
            ],
        ];
        foreach ($bus_types as $key => $value) {
            BusType::create($value);
        }
    }
}
