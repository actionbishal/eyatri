<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => "Administrator",
                'email' => "admin@test.com",
                'password' => bcrypt('admin')
            ],
            [
                'name' => "Test User",
                'email' => "test@test.com",
                'password' => bcrypt('secret')
            ]
        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }
    }
}
