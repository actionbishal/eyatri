<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleTableSeeder::class,
            UserTableSeeder::class,
            UserRoleTableSeeder::class,
            OperatorTableSeeder::class,
            BusTypeTableSeeder::class,
            AmenitiesTableSeeder::class,
            CitiesTableSeeder::class,
            SeatSeeder::class,
            BusTableSeeder::class,
            RouteTableSeeder::class,
            StopTableSeeder::class,
            PermissionTableSeeder::class,
            BookingTableSeeder::class,
        ]);
    }
}
