<?php

use App\Seat;
use App\SeatArrangement;
use Illuminate\Database\Seeder;

class SeatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Hiace seeding
        $seat_name = ['A', 'B', '', 1, '2', '3', '4', '', '5', '6', '7', '', '8', '9', '10', '11', '12', '13'];
        $seat_status = [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1];
        $isCabin = [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


        $seat_arrangement = SeatArrangement::create([
            'bus_type_id' => 2,
            'name' => 'Hiace Arrangement 1',
            'seat_type' => '1X2'
        ]);

        foreach ($seat_name as $key => $value) {
            $seat = Seat::create([
                'seat_arrangement_id' => $seat_arrangement->id,
                'name' => $value,
                'status' => $seat_status[$key],
                'isCabin' => $isCabin[$key],
            ]);
        }

        //deluxe seeding
        $seat_name = ['A', 'B', '',
            'KA', 'KHA', 'C', 'D', '',
            'GA', 'GHA', 'A1','A2', '',
            'B1', 'B2', 'A3', 'A4', '',
            'B3', 'B4', 'A5', 'A6', '',
            'B5', 'B6', 'A7', 'A8', '',
            'B7', 'B8', 'A9', 'A10', '',
            'B9', 'B10', 'A11', 'A12', '',
            'B11', 'B12', 'A13', 'A14', 'A15', 'B13', 'B14'];
        $seat_status = [
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 0, 1, 1,
            1, 1, 1, 1, 1,
           ];

        $isCabin = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $seat_arrangement = SeatArrangement::create([
            'bus_type_id' => 1,
            'name' => 'Deluxe Arrangement 1',
            'seat_type' => '2X2'
        ]);

        foreach ($seat_name as $key => $value) {
            $seat = Seat::create([
                'seat_arrangement_id' => $seat_arrangement->id,
                'name' => $value,
                'status' => $seat_status[$key],
                'isCabin' => $isCabin[$key],
            ]);
        }

    }
}
