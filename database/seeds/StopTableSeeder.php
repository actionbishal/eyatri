<?php

use App\Stop;
use Illuminate\Database\Seeder;

class StopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stop = [
            [
                'bus_id'=>1,
                'city_id'=>1,
                'name' => "New Bus Park",
                'description'=>"Near Makalu Pump",
                'time' => "1:30"
            ],
            [
                'bus_id'=>1,
                'city_id'=>1,
                'name' => "Kalanki",
                'description'=>"Landmark Prabhu Bank",
                'time' => "12:30"
            ],
            [
                'bus_id'=>1,
                'city_id'=>2,
                'name' => "Prithvi Chowk",
                'description'=>"Prabhu Bank",
                'time' => "12:30"
            ],

        ];
        foreach ($stop as $key => $value) {
            Stop::create($value);
        }
    }
}
