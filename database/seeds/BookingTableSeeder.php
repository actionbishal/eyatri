<?php

use App\Booking;
use Illuminate\Database\Seeder;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $booking = [
            [
                'fullname' => 'suman shrestha',
                'email' => "admin@gmial.com ",
                'contact' => "9802355754",
                'age' => "20",
                'amount' => "12000",
                'status' => "1",
                'bus_id' => "1",
                'seats' => [69],
                'traveldate' => "2076-1-9",
                'boarding_point' => "kathmandu",
            ]
        ];
        foreach ($booking as $key => $value) {
            Booking::create($value);
        }
    }
}
