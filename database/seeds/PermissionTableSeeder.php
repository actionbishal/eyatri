<?php

use App\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'create-bus',
                'description' => 'User can create resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'edit-bus',
                'description' => 'User can view resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'delete-bus',
                'description' => 'User can edit resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'create-amenities',
                'description' => 'User can create resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'edit-amenities',
                'description' => 'User can view resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'delete-amenities',
                'description' => 'User can edit resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'create-bustypes',
                'description' => 'User can create resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'edit-bustypes',
                'description' => 'User can view resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name' => 'delete-bustypes',
                'description' => 'User can edit resources',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
        ];
        foreach ($permissions as $key=>$value){
            Permission::create($value);
        }
    }
}
