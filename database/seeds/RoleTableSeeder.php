<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => "Admin",
                'display_name' => "Administrator",
                'description' => 'Administrator'
            ],
            [
                'name' => "Operator",
                'display_name' => "Operator",
                'description' => 'Operator'
            ],
            [
                'name' => "User",
                'display_name' => "User",
                'description' => 'General user'
            ]
        ];

        Role::insert($roles);
    }
}
