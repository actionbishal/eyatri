<?php

use App\Bus;
use Illuminate\Database\Seeder;

class BusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bus = [
            [
                'name' => "Kathmandu Hiace",
                'contact_no' => "9806050253 ",
                'registration_number' => "123",
                'type_id' => "1",
                'operator_id' => "2"
            ],
            [
                'name' => "New Yatayat",
                'contact_no' => "9806050253 ",
                'registration_number' => "123",
                'type_id' => "1",
                'operator_id' => "2"
            ],
        ];
        foreach ($bus as $key => $value) {
            $bus = Bus::create($value);
            $bus->amenities()->attach([1, 2]);
        }
    }
}
