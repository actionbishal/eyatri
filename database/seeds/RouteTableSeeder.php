<?php

use App\Route;
use Illuminate\Database\Seeder;

class RouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $route = [
            [
                'bus_id' => 1,
                'status' => 1,
                'price' => '12000'
            ],
            [
                'bus_id' => 2,
                'status' => 1,
                'price' => '6000'
            ],

        ];
        foreach ($route as $key => $value) {
            $route = Route::create($value);
            $route->cities()->sync([1 => ['time' => '1:00AM'], 2 => ['time' => '2:00AM']]);

        }
    }
}
