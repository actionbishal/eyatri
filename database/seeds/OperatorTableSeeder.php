<?php

use App\Operator;
use App\User;
use Illuminate\Database\Seeder;

class OperatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operators = [
            [
                'name' => "Myagdi Korola",
                'contact_no' => "9806050253",
                'address' => "Kalanki,Kathmandu",
            ],
            [
                'name' => "Sajha Yatayat",
                'contact_no' => "9806050253",
                'address' => "Sitapaila,Kathmandu",
            ]

        ];
        $users = [
            [
                'name' => "Myagdi Korola",
                'email' => 'operator@test.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => "Sajha Yatayat",
                'email' => 'operator1@test.com',
                'password' => bcrypt('secret'),
            ]
        ];

        foreach ($operators as $key => $value) {
            $operator = new Operator($value);
            $user = User::create($users[$key]);
            $user->attachRole(2);
            $operator->user()->associate($user)->save();
        }

    }
}
