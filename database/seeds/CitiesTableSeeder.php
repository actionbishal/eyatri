<?php

use App\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'name' => "Kathmandu"

            ],
            [
                'name' => "Pokhara"

            ],
            [
                'name' => "Narayangarh"

            ],
            [
                'name' => "Butwal"

            ]
        ];

        foreach ($cities as $key => $value) {
            City::create($value);
        }
    }


}
