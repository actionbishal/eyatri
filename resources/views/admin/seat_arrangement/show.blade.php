<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="seat_layout pull-left"
                     @if(!count($seat_arrangement->seats))style="display: none;"@endif>
                    <div class="cabin_seats pull-left">
                        <?php $i = 1 ?>
                        @foreach($seat_arrangement->seats as $seat)
                        @if($seat->isCabin)
                        <span class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>
                                                        <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>
                        </span>
                        @if($i%3==0)<br> @endif
                        <?php $i++ ?>
                        @endif
                        @endforeach
                    </div>
                    <div class="pull-right">
                        <img src="{{asset('admin/img/steering.png')}}"
                             style="margin-bottom:10px;" height="40px" width="40px">
                    </div>
                    <div class="clearfix"></div>
                    <hr style="color: lightblue">

                    @if($seat_arrangement->seat_type =="1X2")
                    <?php $i = 1 ?>
                    @foreach($seat_arrangement->seats as $seat)
                    @if(!$seat->isCabin)
                    <span tabindex="1" class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>
                                                    <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>
                                                    </span>
                    @if($i%4==0)<br> @endif
                    <?php $i++ ?>
                    @endif
                    @endforeach
                    @endif


                    <!-- for 2X2-->

                    @if($seat_arrangement->seat_type =="2X2")
                    <?php $i = 1 ?>
                    @foreach($seat_arrangement->seats as $seat)
                    @if(!$seat->isCabin)
                    <span tabindex="1" class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>

                                                <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>

                                                    </span>
                    @if($i%5==0)<br> @endif
                    <?php $i++ ?>
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <h4>Associated Buses</h4>
        <p>
            @foreach($seat_arrangement->buses as $bus)
            <span>{{$bus->name}}</span>
            @endforeach
        </p>
    </div>
</div>
