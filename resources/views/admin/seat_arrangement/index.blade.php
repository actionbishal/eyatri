@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            View Seat Arrangements
            <small>View Seat Arrangements</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="tab-content">
                        <div class="row panel-heading">
                            <div class="col-sm-2">
                                <a href="{{route('seat-arrangement.create')}}" class="btn btn-default">+ New Seat
                                    Arrangement</a>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade in active">
                            <div class="box-body">
                                <table id="example1"
                                       class="table table-bordered table-striped table-responsive dataTable"
                                       role="grid" aria-describedby="example1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="hidden sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                            rowspan="1" colspan="1"
                                            aria-label="ID: activate to sort column ascending" style="width: 0px;">
                                            ID
                                        </th>
                                        <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"
                                            rowspan="1" colspan="1"
                                            aria-label="Bus Name: activate to sort column ascending"
                                            style="width: 74px;" aria-sort="descending">Arrangement Name
                                        </th>
                                        <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"
                                            rowspan="1" colspan="1"
                                            aria-label="Bus Name: activate to sort column ascending"
                                            style="width: 74px;" aria-sort="descending">Bus Type
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                            rowspan="1" colspan="1"
                                            aria-label="route: activate to sort column ascending"
                                            style="width: 115px;">No. of Cabins
                                        </th>
                                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                            rowspan="1" colspan="1"
                                            aria-label="Bus RegiNumber: activate to sort column ascending"
                                            style="width: 122px;">No. of Back Seats
                                        </th>
                                        <th width="200px;" class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 199px;">Action
                                        </th>
                                        <th width="200px;" class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 199px;">Associated Buses
                                        </th>
                                        <th width="200px;" class="sorting" tabindex="0"
                                            aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                            aria-label="Action: activate to sort column ascending"
                                            style="width: 199px;">Remarks
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($seat_arrangements as $seat_arrangement)
                                        <tr>
                                            <td class=" hidden center sorting_1">{{$seat_arrangement->id}}</td>
                                            <td>{{$seat_arrangement->name}}</td>
                                            <td>{{$seat_arrangement->bus_type->name}}</td>
                                            <td>{{$seat_arrangement->cabin_counts}}</td>
                                            <td>{{$seat_arrangement->back_seats_counts}}</td>
                                            <td>
                                                @if($seat_arrangement->is_operator_seat_arrangement ||
                                                Auth::user()->hasRole('Admin'))
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{route('seat-arrangement.edit', $seat_arrangement->id)}}">
                                                        <i class="fa fa-fw fa-edit"></i></a>
                                                    <a class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                       data-placement="bottom" title="Delete"
                                                       href="{{route('seat-arrangement.destroy',$seat_arrangement->id)}}"
                                                       onclick="destroy(event,this)">
                                                        <i class="fa fa-fw fa-times"></i>
                                                    </a>
                                                @endif
                                                <a class="btn btn-sm bg-olive"
                                                   data-placement="bottom" href="" data-toggle="modal"
                                                   data-target="#myModal" onclick="show({{$seat_arrangement->id}})">
                                                    <i class="fa fa-fw fa-eye"></i></a>
                                                <a class="btn btn-sm btn-info" data-toggle="tooltip"
                                                   data-placement="bottom" title="Duplicate"
                                                   href="{{route('seat-arrangement.duplicate', $seat_arrangement->id)}}">
                                                    <i class="fa fa-fw fa-clipboard"></i></a>
                                            </td>
                                            <td>
                                                @foreach($seat_arrangement->buses as $bus)
                                                <a href="" class="btn"> {{$bus->name}}</a>
                                                @endforeach
                                            </td>
                                            <td>
                                                @if(!$seat_arrangement->is_operator_seat_arrangement)
                                                    You cannot assign this arrangement.you can duplicate it and modify.
                                                @else
                                                    <a type="button" class="btn btn-sm btn-info"
                                                       onclick="assign({{$seat_arrangement->id}})"
                                                       data-toggle="modal" data-target="#assign_modal">
                                                        Assign To Bus
                                                        <i class="fa fa-fw fa-clipboard"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="hidden" rowspan="1" colspan="1">ID</th>
                                        <th rowspan="1" colspan="1">Arrangement Name</th>
                                        <th rowspan="1" colspan="1">Bus Type</th>
                                        <th rowspan="1" colspan="1">No. Of Cabins</th>
                                        <th rowspan="1" colspan="1">No. Of Back Seats</th>
                                        <th rowspan="1" colspan="1">Action</th>
                                        <th rowspan="1" colspan="1">Associated Buses</th>
                                        <th rowspan="1" colspan="1">Remarks</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="modal fade" id="seat_modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><i
                                                            class="fa fa-close"></i>
                                                </button>
                                                <h3 class="text-left">Preview Seat Arrangement</h3>
                                                <p class="text-right">
                                                    <a class="btn btn-sm btn-info"
                                                       data-toggle="tooltip"
                                                       data-placement="bottom" title="Duplicate"
                                                       href="{{route('seat-arrangement.duplicate', $seat_arrangement->id)}}">
                                                        <i class="fa fa-fw fa-clipboard"></i>Duplicate</a>
                                                </p>
                                            </div>
                                            <div class="modal-body">
                                                <div id="seat_layout"></div>
                                            </div>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--                            Assign to bus dialog-->
                                <div class="modal fade" id="assign_modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><i
                                                            class="fa fa-close"></i>
                                                </button>
                                                <h3 class="text-left">Assign Seat Arrangement To Your Bus</h3>
                                            </div>
                                            <div class="modal-body">
                                                <div id="assign_seat"></div>
                                            </div>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });

        function show(id) {
            $.ajax({
                type: 'GET',
                url: 'seat-arrangement/' + id,
                success: function (response) {
                    let seat_modal = $('#seat_modal');
                    $('#seat_layout').html(response);
                    seat_modal.modal('show');
                }
            });
        }

        function assign(id) {
            $.ajax({
                type: 'GET',
                url: 'seat-arrangement/assign/' + id,
                success: function (response) {
                    let assign_modal = $('#assign_modal');
                    $('#assign_seat').html(response);
                    assign_modal.modal('show');
                }
            });
        }

        function destroy(e, $this) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this  file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            _token: "{{ csrf_token() }}",
                            _method: 'delete'
                        },
                        url: $this.href,
                        statusCode: {
                            200: function (response) {
                                $this.closest("tr").remove();

                                swal("Poof! Your  file has been deleted!", "success");
                            },
                        }
                    });
                } else {
                    swal("Your file is safe!");
                }
            });

        }
    </script>
    <!-- /.content -->
@endsection
