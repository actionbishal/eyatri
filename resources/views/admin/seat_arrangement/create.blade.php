@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Seat Arrangements
            <small>Add new seat</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bus management</li>
            <li class="active">Add new bus</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box-warning box">
                    <div class="panel-heading">
                        <div class="alert alert-info">Create new Seat Arrangements
                            <p>
                                Use the form below to start creating your seats layouts.
                            </p>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{route('seat-arrangement.store')}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="card col-sm-6">
                                    <div class="card-title"><h4>Form</h4></div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Seat Arrangement Name</label>
                                                <input name="name" type="text" class="form-control" placeholder="Arrangement 1">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Bus Type</label>
                                                <select tabindex="-1"
                                                        class="form-control select2 required select2-hidden-accessible"
                                                        style="width: 100%;" name="type_id" aria-hidden="true">
                                                    <option></option>
                                                    @foreach($bus_types as $bus_type)
                                                        <option value="{{$bus_type->id}}">{{$bus_type->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No. of Back Seats</label>
                                                <input id="max_seats" type="number" class="form-control" value="45">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6" id="seat_type_item">
                                                <label>Seat Type</label>
                                                <select class="form-control" id="seat_type" name="seat_type">
                                                    <option value="2X2">2X2</option>
                                                    <option value="1X2">1X2</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>No. of Cabin Seats</label>
                                                <input id="cabin_seats" name="cabin" type="number" class="form-control">
                                            </div>
                                            <div class="form-group col-sm-6 pull-right">
                                                <a class="btn btn-default" id="view_layout">View layout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card col-sm-3">
                                    <div class="card-header">
                                        <h4>Seat Layout</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="seat_layout pull-left" style="display: none;">
                                        </div>
                                        <div id="cabin" style="display: none">
                                            <div class="cabin_seats pull-left">
                                            </div>
                                            <div class="pull-right">
                                                <img src="{{asset('admin/img/steering.png')}}"
                                                     style="margin-bottom:10px;" height="40px" width="40px">
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr style="color: lightblue">
                                        </div>
                                    </div>
                                </div>
                                <div id="edit-section" style="display:none">
                                    <div class="card col-sm-3">
                                        <div class="card-header ">
                                            <h4>Edit Seat</h4>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label>Seat Name</label>
                                            <input type="text" class="form-control" id="seat_name" maxlength="3">
                                            <div id="output"></div>

                                        </div>
                                        <div class="form-group col-md-7" id="seat_row">
                                            <label>Seat Label</label>
                                            <select class="form-control" id="seat-status" name="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-footer" style="text-align: left">
                                    <button tabindex="10" type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                        <div id="seat" style="display: none">
                            <span class="fa-stack fa-lg seat-item">
                                <i class="fa img-seat fa-stack-2x"></i>
                                <span class="fa-stack-1x seat_text"></span>
                                <input type="hidden" class="seat_name" name="seat_name[]" value=""/>
                                <input type="hidden" class="seat_status" name="seat_status[]" value="1"/>
                                <input type="hidden" class="isCabin" name="isCabin[]" value="0"/>
                            </span>
                        </div>
                        <div id="seat_not_available" style="display: none">
                            <span class="fa-stack fa-lg seat-item">
                                <i class="fa img-seat-o fa-stack-2x"></i>
                                <span class="fa-stack-1x seat_text"></span>
                                <input type="hidden" class="seat_name" name="seat_name[]" value=""/>
                                <input type="hidden" class="seat_status" name="seat_status[]" value="0"/>
                                <input type="hidden" class="isCabin" name="isCabin[]" value="0"/>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Select --"
            });
            $("#view_layout").on('click', function (e) {
                e.preventDefault();
                let seat_layout = $(".seat_layout"),
                    max_seats = $("#max_seats").val();
                let cabin_seats = $('#cabin_seats').val(),
                    cabin_layout = '',
                    seat = $('#seat');
                seat.find('input.isCabin').val(1);
                for (i = 0; i < cabin_seats; i++) {
                    cabin_layout += seat.html();
                }
                $('.cabin_seats').html(cabin_layout);
                let layout = $('#cabin').html();

                seat.find('input.isCabin').val(0);
                let seat_type = $("#seat_type").val(),
                    seat_item = seat.html(),
                    seat_not_available = $('#seat_not_available').html(),
                    seat_count = 0;

                if (seat_type === "1X2") {
                    // seat_layout.css('width', '180px');
                    let row = max_seats / 3;
                    for (let i = 1; i < row; i++) {
                        if (i < row) {
                            layout += seat_count < max_seats - 4 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 4 ? seat_not_available : "";
                            layout += seat_count < max_seats - 4 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 4 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 4 ? '</br>' : "";
                        }
                    }
                    layout += '</br>' + seat_item + seat_item + seat_item + seat_item;
                    seat_layout.css('display', "block");
                    seat_layout.html(layout);
                }
                if (seat_type === "2X2") {
                    let row = max_seats / 4;
                    for (let i = 1; i < row; i++) {
                        if (i < row) {
                            layout += seat_count < max_seats - 5 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 5 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 5 ? seat_not_available : "";
                            layout += seat_count < max_seats - 5 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 5 ? seat_item : "";
                            seat_count++;
                            layout += seat_count < max_seats - 5 ? '</br>' : "";
                        }
                    }
                    layout += '</br>' + seat_item + seat_item + seat_item + seat_item + seat_item;
                    seat_layout.css('display', "block");
                    seat_layout.html(layout);
                }
                $('.seat-item').on('click', function (e) {
                    let $this = $(this);
                    let edit_section = $('#edit-section');
                    let seat_text = $this.find('.seat_text').text();
                    edit_section.find('#seat_name').val(seat_text);
                    $this.find('i').hasClass('img-seat-o') ?
                        edit_section.find('#seat-status').val("0") :
                        edit_section.find('#seat-status').val("1");
                    edit_section.replaceWith(edit_section).fadeIn().css('display', 'block');
                    edit_section.find('#seat_name').focus();
                    $('#seat-status').on('change', function (e) {
                        let status = $(this).val();
                        if (status === '1') {
                            $this.find('i').removeClass('img-seat-o').addClass('img-seat');
                        } else {
                            $this.find('.seat_text').empty();
                            $this.find('i').removeClass('img-seat').addClass('img-seat-o');
                        }
                        $this.find('input.seat_status').val(status)
                    });
                    $("#seat_name").keyup(function () {
                        let txtVal = $(this).val();
                        $("#output").text("You have entered " + txtVal);
                        $this.find('.seat_text').text(txtVal);
                        $this.find('input.seat_name').val(txtVal);
                    });
                })
            })
        });
    </script>
    <!-- /.content -->
@endsection
