<div class="row">
    <form role="form" action="{{route('seat-arrangement.assign')}}" method="post" class="validate"
          enctype="multipart/form-data">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body assign_seat">
                    <label>Assign <b>{{$seat_arrangement->name}}</b> to:</label><br>
                    <label for="exampleInputEmail1">Bus Name</label>
                    <select tabindex="-1"
                            class="form-control select2 required "
                            name="bus_id" aria-hidden="false"
                            id="bus">
                        <option></option>
                        @foreach($buses as $bus)
                            <option value="{{$bus->id}}">
                                {{$bus->name}}/{{$bus->registration_number}}
                            </option>
                        @endforeach
                    </select>
                    <input type="hidden" value="{{$seat_arrangement->id}}" name="seat_arrangement_id">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box-footer" >
                <button tabindex="10" type="submit" class="btn btn-info pull-right">Save Changes</button>
            </div>
        </div>
        {{csrf_field()}}
    </form>
</div>
