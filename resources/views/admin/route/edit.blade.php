@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Route Management
            <small>Update Route</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Route management</li>
            <li class="active">Update Route</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box-warning box">
                    <div class="panel-body route-panel">
                        <div class="alert alert-info"><i class="fa fa-info-circle"></i> Update Route
                            <p>Use the form below to update the selected route. You can delete locations or re-order
                                them using the icons that show at the end of each location row on mouse over.</p>
                        </div>
                        <form role="form" action="{{route('route.update',[$route->id])}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put"/>
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group" row>
                                        <label class="col-sm-2">Bus Name</label>
                                        <div class="col-sm-6">

                                            <select tabindex="-1"
                                                    class="form-control select2 required "
                                                    name="bus_id" aria-hidden="false"
                                                    id="bus">
                                                {{$buses}}
                                                @foreach($buses as $bus)
                                                    <option value="{{$bus->id}}"
                                                            {{$bus->id == $route->bus->id ? "selected":""}}>
                                                        {{$bus->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="ui-sortable locations">
                                    @foreach($route->cities as $index => $location)
                                        <div class="form-group row location-row">
                                            <label class="col-sm-2">Location {{$index+1}}:</label>
                                            <div class="col-sm-6">
                                                <select tabindex="-1" data-index="{{$index + 1}}"
                                                        class="form-control select2 city select2-hidden-accessible"
                                                        style="width: 100%;" name="city[]" aria-hidden="true">
                                                    <option></option>
                                                    @foreach($cities as $city)
                                                        <option value="{{$city->id}}"
                                                                {{$city->id == $location->id ? "selected":""}}>{{$city->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="location-icons" style="right: 2px">
                                                <a href="javascript:void(0);"
                                                   class="fa fa-times location-delete-icon"
                                                   onclick="destroy(event,this)"></a>
                                                <a href="javascript:void(0);"
                                                   class="location-move-icon fa fa-bars"></a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div id="location_clone" style="display: none">
                                    <div class="form-group row location-row">
                                        <label class="col-sm-2">Location {ORDER}:</label>
                                        <div class="col-sm-6">
                                            <select tabindex="-1" data-index="{INDEX}"
                                                    class="form-control select2 city select2-hidden-accessible"
                                                    style="width: 100%;" aria-hidden="true">
                                                <option></option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="location-icons" style="right: 2px">
                                            <a href="javascript:void(0);"
                                               class="fa fa-times location-delete-icon"
                                               onclick="destroy(event,this)"></a>
                                            <a href="javascript:void(0);"
                                               class="location-move-icon fa fa-arrows-alt"></a>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                {{--<div class="col-sm-2 col-sm-offset-2">--}}
                                {{--<a href="#" class="btn btn-default add-btn">Add + </a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group has-feedback row">
                                    <label for="exampleInputEmail1" class="col-sm-2">Price(Rs.)</label>
                                    <div class="col-sm-6">
                                        <input tabindex="4" type="text" class="form-control required" name="price"
                                               data-parsley-trigger="change" required value="{{$route->price}}"
                                               placeholder=Price">
                                        <span class="form-control-feedback"></span>
                                    </div>

                                </div>
                                <div class="form-group has-feedback row">
                                    <label for="exampleInputEmail1" class="col-sm-2">Arrival Time</label>
                                    <div class="col-sm-6">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text"
                                                   class="form-control input-small timepicker1"
                                                   name="arrival_time"
                                                   required value="{{$route->arrival_time}}"
                                            >
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group has-feedback row">
                                    <label for="exampleInputEmail1" class="col-sm-2">Departure Time</label>
                                    <div class="col-sm-6">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text"
                                                   class="form-control input-small timepicker1"
                                                   name="departure_time"
                                                   required value="{{$route->departure_time}}"
                                            >
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row box-footer">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <button tabindex="10" type="submit" class="btn btn-default">Save</button>
                                    <a tabindex="10" href="{{route('route.index')}}" class="btn btn-default">Cancel</a>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <div class="ui-widget-overlay ui-front" style="display: none"></div>
        <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons"
             tabindex="-1" role="dialog" aria-describedby="dialogPrompt" aria-labelledby="ui-id-1"
             style="display: none;">
            <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span
                        id="ui-id-1" class="ui-dialog-title">Same Location</span>
                <button type="button"
                        class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close"
                        role="button" aria-disabled="false" title="close"><span
                            class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span
                            class="ui-button-text">close</span></button>
            </div>
            <div id="dialogPrompt" style="" class="ui-dialog-content ui-widget-content">
                <p>The location was already chosen. Please select another one.</p>
            </div>
            <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
                <div class="ui-dialog-buttonset">
                    <button type="button"
                            class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
                            role="button" aria-disabled="false"><span class="ui-button-text">OK</span>
                    </button>
                </div>
            </div>
        </div>
    </section>
    <script>
        (function (document) {
            $(document).ready(function () {
                $('.timepicker1').timepicker({
                    use24hours: true,
                    format: 'HH:mm'
                });
                let locations = $(".locations");
                let $dialogPrompt = $("#dialogPrompt");

                let init_select2 = function () {
                    //Initialize Select2 Elements
                    locations.find('.select2').select2({
                        placeholder: "-- Select city --",
                    }).on('select2:select', function (e) {
                        let $this = $(this);
                        locations.find('.city').each((order, ele) => {
                            if ($(ele).val() === $this.val() && $this.data('index') !== order + 1) {
                                $this.val("").trigger('change');
                                $dialogPrompt.dialog('open');
                            }
                        });
                    });
                };

                let init_sortable = function () {
                    locations.sortable({

                        axis: 'y',
                        // opacity: 0.7,
                        handle: '.location-move-icon',
                        update: function (e) {
                            locations.find(".location-row").each(function (order, row) {
                                let title = "Location " + (order + 1) + ":";
                                $(row).find('label').html(title);
                            });
                        },
                        tolerance: 'pointer',
                        cursor: 'move',
                        containment: 'parent',
                        connectWith: ".locations",
                    });
                };
                init_select2();
                init_sortable();
                $(".add-btn").on('click', function (e) {
                    e.preventDefault();
                    locations.children('select').select2('destroy').end();
                    let location_clone = $('#location_clone');
                    location_clone.find('select').attr('name', "city[]");
                    let clone_text = location_clone.html();
                    let number_of_locations = locations.find(".location-row").length,
                        order = parseInt(number_of_locations, 10) + 1;
                    clone_text = clone_text.replace(/\{ORDER\}/g, order);
                    clone_text = clone_text.replace(/\{INDEX\}/g, order);
                    $(".ui-sortable").append(clone_text);
                    init_select2();
                    location_clone.find('select').attr('name', "");
                });
                if ($dialogPrompt.length > 0) {
                    $dialogPrompt.dialog({
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        modal: true,
                        buttons: (function () {
                            let buttons = {};

                            buttons = function () {
                                $dialogPrompt.dialog('close');
                            };

                            return buttons;
                        })()
                    });
                }
            });
        })(document);

        function destroy(e, $this) {
            e.preventDefault();
            $this.closest('.location-row').remove();
            $('.locations').find(".location-row").each(function (order, index) {
                    order++;
                    $(this).find('label').text('Location ' + order + ':')
                }
            )
        }
    </script>
    <!-- /.content -->
@endsection
