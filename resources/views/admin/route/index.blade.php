@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            View Route Management Details
            <small>View Route Management Details</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="tab-content">
                        <div class="row panel-heading">
                            <div class="col-sm-2">
                                <a href="{{route('route.create')}}" class="btn btn-default">+ Add Route</a>
                            </div>
                        </div>
                        <div id="home" class="tab-pane fade in active">
                            <div class="box-body">
                                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="example1" class="table table-bordered table-striped dataTable"
                                                   role="grid" aria-describedby="example1_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="hidden sorting" tabindex="0"
                                                        aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        aria-label="ID: activate to sort column ascending"
                                                        style="width: 0px;">
                                                        ID
                                                    </th>
                                                    <th class="sorting_desc" tabindex="0"
                                                        aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Route Name: activate to sort column ascending"
                                                        style="width: 74px;" aria-sort="descending">Bus Name
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        style="width: 122px;">From Place
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        style="width: 68px;">To Place
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        style="width: 68px;">Locations
                                                    </th><th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                             rowspan="1" colspan="1"
                                                             style="width: 68px;">Price(Rs.)
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        style="width: 68px;">Arrival Time
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        style="width: 68px;">Departure Time
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                                        rowspan="1" colspan="1"
                                                        aria-label="Maximum Seats: activate to sort column ascending"
                                                        style="width: 115px;">Status
                                                    </th>

                                                    <th width="200px;" class="sorting" tabindex="0"
                                                        aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                                        aria-label="Action: activate to sort column ascending"
                                                        style="width: 199px;">Action
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($routes as $route)
                                                    <tr role="row" class="odd" id="row_{{$route->id}}}">
                                                        <td class=" hidden center sorting_1">{{$route->id}}</td>
                                                        <td class="center">{{$route->bus->name}}({{$route->bus->registration_number}})</td>
                                                        @if($route->cities)
                                                        <td class="center">
                                                            {{$route->cities->first()->name}}
                                                        </td>
                                                        @endif
                                                        <td class="center">{{$route->cities->last()->name}}</td>
                                                        <?php $locations = [] ?>
                                                        @foreach($route->cities  as $city)
                                                            <?php array_push($locations, $city->name);?>
                                                        @endforeach
                                                        <td class="center">{{implode(' - ', $locations)}}</td>
                                                        <td class="center">{{$route->price}}</td>
                                                        <td class="center">{{$route->arrival_time}}</td>
                                                        <td class="center">{{$route->departure_time}}</td>
                                                        <td>
                                                            @if($route->status)
                                                                <a class="btn btn-sm btn-success"
                                                                   href="{{route("route.toggle", $route->id)}}">
                                                                    <i class="fa fa-fw fa-check"></i>Active
                                                                </a>
                                                            @else
                                                                <a class="btn btn-sm btn-danger"
                                                                   href="{{route("route.toggle", $route->id)}}">
                                                                    <i class="fa fa-fw fa-spinner"></i>Inactive
                                                                </a>
                                                            @endif
                                                        </td>
                                                        <td class="center">
                                                            <a class="btn btn-sm bg-olive" data-toggle="tooltip" data-placement="bottom" title="View" href="" data-toggle="modal"
                                                               data-target="#myModal" onclick="setValue({{$route}})">
                                                                <i class="fa fa-fw fa-eye"></i></a>

                                                            <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"
                                                               href="{{route("route.edit", $route->id)}}">
                                                                <i class="fa fa-fw fa-edit"></i></a>
                                                            <a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"
                                                               href="{{route('route.destroy',$route->id)}}"
                                                               onclick="destroy(event,this)">
                                                                <i class="fa fa-fw fa-times"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th class="hidden" rowspan="1" colspan="1">ID</th>
                                                    <th rowspan="1" colspan="1">Bus Name</th>
                                                    <th rowspan="1" colspan="1">From Place</th>
                                                    <th rowspan="1" colspan="1">To Place</th>
                                                    <th rowspan="1" colspan="1">Locations</th>
                                                    <th rowspan="1" colspan="1">Price</th>
                                                    <th rowspan="1" colspan="1">Arrival</th>
                                                    <th rowspan="1" colspan="1">Departure</th>
                                                    <th rowspan="1" colspan="1">Status</th>
                                                    <th rowspan="1" colspan="1">Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View Route Details</h4>
                    </div>
                    <div class="modal-body">
                        <b></b><b>Bus Name</b>
                        <p class="bus_name"></p>
                        <b></b><b>Boarding Point</b>
                        <p class="boarding_point"></p>
                        <b></b><b>Drop Point</b>
                        <p class="drop_point"></p>
                        <b></b><b>Price</b>
                        <p class="price"></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>

        function setValue(route) {
            $(".bus_name").html(route.bus.name);
            $(".boarding_point").html(route.start_point);
            $(".drop_point").html(route.end_point);
            $(".price").html(route.price);
        }
        $(function () {
            $("#example1").DataTable();
        });
        function destroy(e, $this) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this  file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            _token: "{{ csrf_token() }}",
                            _method: 'delete'
                        },
                        url: $this.href,
                        statusCode: {
                            200: function (response) {
                                $this.closest("tr").remove();

                                swal("Poof! Your  file has been deleted!", "success");
                            },
                        }
                    });
                }
                else {
                    swal("Your file is safe!");
                }
            });

        }
    </script>
    <!-- /.content -->
@endsection
