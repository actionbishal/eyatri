@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Route Management
            <small>Add new Route</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Route management</li>
            <li class="active">Add new Route</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box-warning box">
                    <div class="panel-body">
                        <div class="alert alert-info">Add new Route</div>
                        <form role="form" action="{{route('route.store')}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="col-sm-6">
                                        <label>Bus Name</label>
                                        <select tabindex="-1"
                                                class="form-control select2 required "
                                                name="bus_id" aria-hidden="false"
                                                id="bus">
                                            @foreach($buses as $bus)
                                                <option value="{{$bus->id}}">
                                                    {{$bus->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('bus_id'))
                                            <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 {{ $errors->has('price') ? ' has-error' : '' }}">
                                        <label>Price(Rs.)</label>
                                        <input tabindex="4" type="text" class="form-control required" name="price"
                                               data-parsley-trigger="change"
                                               placeholder="Price for the route">
                                        @if ($errors->has('price'))
                                            <span class="help-block form-control-feedback ">
                                    <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group row ">
                                    <div class="col-sm-6">
                                        <label>From place</label>
                                        <select tabindex="-1"
                                                class="form-control select2 required select2-hidden-accessible"
                                                style="width: 100%;" name="city[]" aria-hidden="true">
                                            <option value="">-- Select city --</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label></label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text"
                                                   class="form-control input-small timepicker1"
                                                   name="time[]">
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback row">
                                    <div class="col-sm-6 {{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label>To place</label>
                                        <select tabindex="-1"
                                                class="form-control select2 required select2-hidden-accessible"
                                                style="width: 100%;" name="city[]" aria-hidden="true">
                                            <option value="">-- Select city --</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label></label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text"
                                                   class="form-control input-small timepicker1"
                                                   name="time[]">
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </div>
                                </div>

                                {{--<div id="reverse_route" style="display:none">--}}
                                {{--<h4><strong> Create Reverse Route</strong></h4>--}}
                                {{--<div class="form-group row ">--}}
                                {{--<div class="col-sm-6 {{ $errors->has('city') ? ' has-error' : '' }}">--}}
                                {{--<label>From place</label>--}}
                                {{--<select tabindex="-1"--}}
                                {{--class="form-control select2 required select2-hidden-accessible"--}}
                                {{--style="width: 100%;" name="city[]" aria-hidden="true">--}}
                                {{--<option value="">-- Select city --</option>--}}
                                {{--@foreach($cities as $city)--}}
                                {{--<option value="{{$city->id}}">{{$city->name}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6 {{ $errors->has('city') ? ' has-error' : '' }}">--}}
                                {{--<label>To place</label>--}}
                                {{--<select tabindex="-1"--}}
                                {{--class="form-control select2 required select2-hidden-accessible"--}}
                                {{--style="width: 100%;" name="city[]" aria-hidden="true">--}}
                                {{--<option value="">-- Select city --</option>--}}
                                {{--@foreach($cities as $city)--}}
                                {{--<option value="{{$city->id}}">{{$city->name}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group has-feedback row ">--}}
                                {{--<div class="col-sm-6 {{ $errors->has('price') ? ' has-error' : '' }}">--}}
                                {{--<label>Price(Rs.)</label>--}}
                                {{--<input tabindex="4" type="text" class="form-control required"--}}
                                {{--name="price"--}}
                                {{--data-parsley-trigger="change"--}}
                                {{--placeholder="Price for the route">--}}
                                {{--@if ($errors->has('price'))--}}
                                {{--<span class="help-block form-control-feedback ">--}}
                                {{--<strong>{{ $errors->first('price') }}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-6 {{ $errors->has('departure_time') ? ' has-error' : '' }}">--}}
                                {{--<label>Departure Time</label>--}}
                                {{--<div class="input-group bootstrap-timepicker timepicker">--}}
                                {{--<input type="text"--}}
                                {{--class="form-control input-small timepicker1"--}}
                                {{--name="departure_time">--}}
                                {{--<span class="input-group-addon"><i--}}
                                {{--class="glyphicon glyphicon-time"></i></span>--}}
                                {{--</div>--}}
                                {{--@if ($errors->has('departure_time'))--}}
                                {{--<span class="help-block form-control-feedback ">--}}
                                {{--<strong>{{ $errors->first('departure_time') }}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row box-footer">
                                <div class="col-sm-2">
                                    <button tabindex="10" type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
            $('.timepicker1').timepicker({
                use24hours: true,
                format: 'HH:mm'
            });
        });
        $(document).ready(function () {
            $('.direction_type').on('change', function () {
                if (this.value == '2') {
                    $("#reverse_route").show();
                } else {
                    $("#reverse_route").hide();
                }
            });
        });
    </script>
    <!-- /.content -->
@endsection
