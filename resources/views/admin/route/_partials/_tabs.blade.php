<ul class="nav nav-pills">
    <li class="{{ Request::path() == 'admin/route' ? 'active' : '' }}"><a  href="{{route('route.index')}}">Routes</a></li>
    <li class="{{ Request::is('admin/city*') ? 'active' : '' }}"><a href="{{route('city.index')}}">Cities</a></li>
</ul>