@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            View Booking Details
            <small>View Booking Details</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">View Booking Details</h3>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped datatable">
                            <thead>
                            <tr>
                                <th class="hidden">ID</th>
                                <th>Booking ID</th>
                                <th>Customer</th>
                                <th>Bus Name</th>
                                <th>Contact</th>
                                <th>Booked Date</th>
                                <th>Seat</th>
                                <th>Amount(Rs.)</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bookings as $booking)
                                <tr role="row" class="table-row">
                                    <td class=" hidden center sorting_1">{{$booking->id}}</td>
                                    <td class="center">{{$booking->id}}</td>
                                    <td class="center">{{$booking->fullname}}</td>
                                    <td class="center">{{$booking->bus->name}}</td>
                                    <td class="center">{{$booking->email}}/{{$booking->contact}}</td>
                                    <td class="center">{{$booking->traveldate}}</td>
                                    <td class="center">{{ implode(', ', $booking->seats)}}
                                    </td>
                                    <td class="center">{{$booking->amount}}</td>
                                    <td>
                                        @if($booking->status)
                                            <i class="fa fa-fw fa-check"></i>Completed
                                            </a>
                                        @else
                                            <i class="fa fa-fw fa-spinner"></i>Pending
                                            </a>
                                        @endif
                                    </td>
                                    <td class="center">
                                        <a class="btn btn-sm bg-olive" data-toggle="tooltip" data-placement="bottom"
                                           title="View" href="" data-toggle="modal"
                                           data-target="#myModal" onclick="">
                                            <i class="fa fa-fw fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th class="hidden">ID</th>
                                <th>Booking ID</th>
                                <th>Customer</th>
                                <th>Bus Name</th>
                                <th>Contact</th>
                                <th>Booked Date</th>
                                <th>Seat</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <!-- /.content -->
@endsection
