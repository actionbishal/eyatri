@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <?php
    use App\Booking;
    use App\Bus;
    use App\Operator;
    use App\Role;
    use App\Route;

    if (Auth::user()->hasRole('Operator')) {
        $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
        $buses = Bus::where('operator_id', $operator_id)->get();
    } else {
        $buses = Bus::all();
    }

    if (Auth::user()->hasRole('Operator')) {
        $operator_id = Operator::where('user_id', Auth::user()->id)->first()->id;
        $routes = Route::whereHas('bus', function ($query) use ($operator_id) {
            $query->where('operator_id', $operator_id);
        })->get();
    } else {
        $routes = Route::all();
    }

    $countbus = Bus::all();
    $countroute = Route::all();
    $countbookings = Booking::all();
    $countoperators = Operator::all();
    $countcustomers = Role::where('name', '=', 'user');
    $countroute = Route::all()
    ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-bus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Buses</span>
                        <span class="info-box-number"><a
                                    href="">
                                {{$countbus->count()}}
                            </a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-arrows-v"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Routes</span>
                        <span class="info-box-number"><a
                                    href="">
                                    {{$countroute->count()}}
                            </a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Bookings</span>
                        <span class="info-box-number"><a
                                    href="">
                                {{$countbookings->count()}}
                            </a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Customer</span>
                        <span class="info-box-number"><a
                                    href="">
                                {{$countcustomers->count()}}
                            </a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Operators</span>
                        <span class="info-box-number"><a
                                    href="">
                                {{$countoperators->count()}}
                            </a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-arrow-right"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bus Operating Today</span>
                        <span class="info-box-number"><a
                                    href="">4</a></span>
                    </div>
                </div>
            </div>
        </div>
        <hr style="height:1px" color="#607d8b">
        <div class="row">
            <div class="col-md-6">
                <h3> New Route Details</h3>
                <table id="" class="table table-bordered table-striped dataTable"
                       role="grid" aria-describedby="example1_info">
                    <thead>
                    <tr role="row">
                        <th class="hidden sorting" tabindex="0"
                            aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="ID: activate to sort column ascending"
                            style="width: 0px;">
                            ID
                        </th>
                        <th class="sorting_desc" tabindex="0"
                            aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="Route Name: activate to sort column ascending"
                            style="width: 74px;" aria-sort="descending">Bus Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            style="width: 68px;">Locations
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            style="width: 68px;">Price(Rs.)
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            style="width: 68px;">Arrival Time
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            style="width: 68px;">Departure Time
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="Maximum Seats: activate to sort column ascending"
                            style="width: 115px;">Status
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($routes->slice(0, 3)->reverse() as $route)
                        <tr role="row" class="odd" id="row_{{$route->id}}}">
                            <td class=" hidden center sorting_1">{{$route->id}}</td>
                            <td class="center">{{$route->bus->name}}({{$route->bus->registration_number}})</td>
                            <?php $locations = [] ?>
                            @foreach($route->cities  as $city)
                                <?php array_push($locations, $city->name);?>
                            @endforeach
                            <td class="center">{{implode(' - ', $locations)}}</td>
                            <td class="center">{{$route->price}}</td>
                            <td class="center">{{$route->arrival_time}}</td>
                            <td class="center">{{$route->departure_time}}</td>
                            <td>
                                @if($route->status)
                                    <p>
                                        <i class="fa fa-fw fa-check"></i>Active
                                    </p>
                                @else
                                    <i class="fa fa-fw fa-times"></i>Inactive
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="1" colspan="1">Bus Name</th>
                        <th rowspan="1" colspan="1">Locations</th>
                        <th rowspan="1" colspan="1">Price</th>
                        <th rowspan="1" colspan="1">Arrival</th>
                        <th rowspan="1" colspan="1">Departure</th>
                        <th rowspan="1" colspan="1">Status</th>
                    </tr>
                    </tfoot>
                </table>
                <a class="btn btn-sm btn-success"
                   href="{{route('route.index')}}">
                    <i class="fa fa-fw fa fa-arrow-right"></i>View Full List
                </a>
            </div>
            <div class="col-md-6">
                <h3> Recently Added Bus Details</h3>
                <table id="example1"
                       class="table table-bordered table-striped table-responsive dataTable"
                       role="grid" aria-describedby="example1_info">
                    <thead>
                    <tr role="row">
                        <th class="hidden sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="ID: activate to sort column ascending" style="width: 0px;">
                            ID
                        </th>
                        @if(Auth::user()->hasRole('Admin'))
                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                rowspan="1" colspan="1"
                                aria-label="route: activate to sort column ascending"
                                style="width: 115px;">Operator Name
                            </th>
                        @endif
                        <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="Bus Name: activate to sort column ascending"
                            style="width: 74px;" aria-sort="descending">Bus Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="route: activate to sort column ascending"
                            style="width: 115px;">Route Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="Bus Type: activate to sort column ascending"
                            style="width: 68px;">Bus Type
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                            rowspan="1" colspan="1"
                            aria-label="Bus Type: activate to sort column ascending"
                            style="width: 68px;">Status
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($buses->slice(0, 3)->reverse() as $bus)
                        <tr role="row" class="table-row">
                            @if(Auth::user()->hasRole('Admin'))
                                <td class="center">{{$bus->operator->name}}</td>
                            @endif
                            <td class="center">{{$bus->name}}/{{$bus->registration_number}}</td>
                            <td class="center">
                                @if($bus->route)
                                    <a href="{{route('route.edit',$bus->route->id)}}">{{$bus->route->name}}
                                        <?php $locations = [] ?>
                                        @foreach($bus->route->cities  as $cities)
                                            <?php array_push($locations, $cities->name);?>
                                        @endforeach
                                        <p class="route-locations">({{implode(' - ', $locations)}})</p>
                                    </a>
                                @else
                                    No Routes
                                @endif
                            </td>
                            <td class="center">{{$bus->busType->name}}</td>
                            <td>
                                @if($bus->status)
                                    <p>
                                        <i class="fa fa-fw fa-check"></i>Active
                                    </p>
                                @else
                                    <p>
                                        <i class="fa fa-fw fa-times"></i>Inactive
                                    </p>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        @if(Auth::user()->hasRole('Admin'))
                            <th rowspan="1" colspan="1">Operator</th>
                        @endif
                        <th rowspan="1" colspan="1">Bus Name</th>
                        <th rowspan="1" colspan="1">Route Name</th>
                        <th rowspan="1" colspan="1">Bus Type</th>
                        <th rowspan="1" colspan="1">Status</th>
                    </tr>
                    </tfoot>
                </table>
                <a class="btn btn-sm btn-success "
                   href="{{route('bus.index')}}">
                    <i class="fa fa-fw fa fa-arrow-right "></i>View Full List
                </a>
            </div>
        </div>
    </section>
    <script>

    </script>
    <!-- /.content -->
@endsection