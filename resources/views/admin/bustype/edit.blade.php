@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Edit Bus Type Details
            <small>Edit Bus Type</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box box-warning">
                    <div class="panel-heading">
                        <h3>Edit Bus Type</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{ route('bus-type.update',[$bus_type->id]) }}" method="post"
                              class="validate"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put"/>

                            <div class="row">
                                <div class="form-group has-feedback col-sm-12">
                                    <label for="exampleInputEmail1">Bus Type Name</label>
                                    <input class="form-control required" tabindex="1"
                                           data-parsley-trigger="change" data-parsley-minlength="2"
                                           data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                           required="" name="name" value="{{$bus_type->name}}">
                                    <span class="glyphicon  form-control-feedback"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-footer" style="text-align: left">
                                    <button tabindex="10" type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
        $("#view_layout").on('click', function (e) {
            e.preventDefault();
            let seat_layout = $(".seat_layout"),
                max_seats = $("#max_seats").val();
            let seat_type = $("#seat_type").val(),
                seat = $('#seat').html(),
                seat_not_available = $('#seat_not_available').html(),
                seat_count = 0;

            let cabin_seats = $('#cabin_seats').val(),
                cabin_layout = '';
            for (i = 0; i < cabin_seats; i++) {
                cabin_layout += seat;
            }
            $('.cabin_seats').html(cabin_layout);
            let layout = $('#cabin').html();

            if (seat_type === "1X2") {
                seat_layout.css('width', '180px');
                let row = max_seats / 3;
                for (let i = 1; i < row; i++) {
                    if (i < row) {
                        layout += seat_count < max_seats - 4 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 4 ? seat_not_available : "";
                        layout += seat_count < max_seats - 4 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 4 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 4 ? '</br>' : "";
                    }
                }
                layout += '</br>' + seat + seat + seat + seat;
                seat_layout.css('display', "block");
                seat_layout.html(layout);
            }
            if (seat_type === "2X2") {
                let row = max_seats / 4;
                for (let i = 1; i < row; i++) {
                    if (i < row) {
                        layout += seat_count < max_seats - 5 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 5 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 5 ? seat_not_available : "";
                        layout += seat_count < max_seats - 5 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 5 ? seat : "";
                        seat_count++;
                        layout += seat_count < max_seats - 5 ? '</br>' : "";
                    }
                }
                layout += '</br>' + seat + seat + seat + seat + seat;
                seat_layout.css('display', "block");
                seat_layout.html(layout);
            }
            $('.seat-item').on('click', function (e) {
                let $this = $(this);
                let edit_section = $('#edit-section');
                let seat_text = $this.find('.seat_text').text();
                edit_section.find('#seat_name').val(seat_text);
                $this.find('i').hasClass('fa-square-o') ?
                    edit_section.find('#seat-status').val("0") :
                    edit_section.find('#seat-status').val("1");
                edit_section.replaceWith(edit_section).fadeIn().css('display', 'block');
                $('#seat-status').on('change', function (e) {
                    let status = $(this).val();
                    if (status === '1') {
                        $this.find('i').removeClass('fa-square-o').addClass('fa-square')
                    } else {
                        $this.find('.seat_text').empty();
                        $this.find('i').removeClass('fa-square').addClass('fa-square-o')
                    }
                });
                $("#seat_name").keyup(function () {
                    let txtVal = $(this).val();
                    $("#output").text("You have entered " + txtVal);
                    $this.find('.seat_text').text(txtVal);
                });
            })
        })

    </script>
    <!-- /.content -->
@endsection
