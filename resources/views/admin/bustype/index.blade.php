@extends('admin.layout.master')
@section('content-header')
    <section class="content-header">
        <h1>
            Bus Type Details
            <small>Add new Bus Type</small>
        </h1>
    </section>
@endsection
@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box box-warning">
                    <div class="panel-body">
                        <form role="form" action="{{route('bus-type.store')}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <label for="exampleInputEmail1">Bus Type Name</label>
                                    <input type="text" class="form-control required" tabindex="1"
                                           data-parsley-trigger="change" data-parsley-minlength="2"
                                           data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                           required="" name="name" placeholder="Bus Name">
                                    <span class="glyphicon  form-control-feedback"></span>
                                </div>
                                <div class="box-footer">
                                    <button tabindex="10" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">View BusType Details</h3>
                    </div>
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable"
                                           role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending"
                                                style="width: 182px;">Bus Type Name
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending"
                                                style="width: 224px;">Status
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                                style="width: 199px;">Actions
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bus_types as $bus_type)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{$bus_type->name}}</td>
                                                <td>
                                                    @if($bus_type->status)
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="Active"
                                                           href="{{route("bus-type.toggle", $bus_type->id)}}">
                                                            <i class="fa fa-fw fa-check"></i>Active
                                                        </a>
                                                    @else
                                                        <a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Inactive"
                                                           href="{{route("bus-type.toggle", $bus_type->id)}}">
                                                            <i class="fa fa-fw fa-times"></i>Inactive
                                                        </a>
                                                    @endif
                                                </td>
                                                <td class="center">
                                                    <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"
                                                       href="{{route("bus-type.edit", $bus_type->id)}}">
                                                        <i class="fa fa-fw fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Bus Type Name</th>
                                            <th rowspan="1" colspan="1">Status</th>
                                            <th rowspan="1" colspan="1">Actions</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <!-- /.content -->
@endsection
