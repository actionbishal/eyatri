<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Side_useruser panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('admin/img/avatar5.png')}}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Hi, {{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="">
                <a href="{{route('home')}}">
                    <i class="fa fa-angle-double-left"></i> <span>Back To Site</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-clipboard"></i> <span>Dashboard</span>
                </a>
            </li>
            @if(Auth::user()->hasRole('Admin'))
<!--                <li class="treeview">-->
<!--                    <a href="#">-->
<!--                        <i class="fa fa-user"></i>-->
<!--                        <span>Access Management</span>-->
<!--                        <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </a>-->
<!--                    <ul class="treeview-menu">-->
<!--                        <li class="active"><a href="{{route('user.index')}}"><i class="fa fa-angle-double-right"></i>-->
<!--                                User Management</a></li>-->
<!--                        <li><a href="{{route('role.index')}}"><i class="fa fa-angle-double-right"></i> Role-->
<!--                                Management</a></li>-->
<!---->
<!--                    </ul>-->
<!--                </li>-->
                <li class="{{ request()->is('admin/amenities*') ? 'active' : '' }}">
                    <a href="{!! route('amenities.index')!!}">
                        <i class="fa fa-tags"></i>
                        <span>Amenities</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/bus-type*') ? 'active' : '' }}">
                    <a href="{!! route('bus-type.index')!!}">
                        <i class="fa fa-bus"></i>
                        <span>Bus Types</span>
                    </a>
                </li>
                <li class="{{ request()->is('admin/city*') ? 'active' : '' }}">
                    <a href="{!! route('city.index')!!}">
                        <i class="fa fa-building"></i>
                        <span>Cities</span>
                    </a>
                </li>
            <li class="{{ request()->is('admin/operators*') ? 'active' : '' }}">
                <a href="{!! route('operator.index')!!}">
                    <i class="fa fa-user"></i>
                    <span>Operators</span>
                </a>
            </li>
            @endif
            <li class="{{ request()->is('admin/seat-arrangement*') ? 'active' : '' }}">
                <a href="{{route('seat-arrangement.index')}}">
                    <i class="fa fa-crosshairs"></i>
                    <span>Seat Arrangements</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/bus') || request()->is('admin/bus/*') ? 'active' : '' }}">
                <a href="{!! route('bus.index')!!}">
                    <i class="fa fa-bus"></i>
                    <span>Bus Management</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/route*') ? 'active' : '' }}">
                <a href="{!! route('route.index')!!}">
                    <i class="fa fa-arrows-v"></i>
                    <span>Route Management</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/booking*') ? 'active' : '' }}">
                <a href="{{route('booking.index')}}">
                    <i class="fa fa-book"></i>
                    <span>Booking Details</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-crosshairs"></i>
                    <span>Promo Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="{{route('bus.index')}}"><i class="fa fa-angle-double-right"></i> View all</a>
                    </li>
                    <li>
                        <a href="{{route('bus.create')}}"><i class="fa fa-angle-double-right"></i> Add new</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/gallery*') ||request()->is('admin/gallery*')  ? 'active' : '' }}">
                <a href="{!! route('gallery.index')!!}">
                    <i class="glyphicon glyphicon-picture"></i>
                    <span> Gallery</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-asterisk"></i>
                    <span>Agents</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="{{route('bus.index')}}"><i class="fa fa-angle-double-right"></i> View all</a>
                    </li>
                    <li>
                        <a href="{{route('bus.create')}}"><i class="fa fa-angle-double-right"></i> Add new</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Customers</span>

                </a>
                <ul class="treeview-menu"></ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-ban"></i>
                    <span>Cancellation</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="active">
                        <a href="{{route('bus.index')}}"><i class="fa fa-angle-double-right"></i> View all</a>
                    </li>
                    <li>
                        <a href="{{route('bus.create')}}"><i class="fa fa-angle-double-right"></i> Add new</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>Enviroment Variables</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span>Settings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-star"></i> <span>Ratings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
