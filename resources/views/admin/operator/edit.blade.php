@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Edit Operator
            <small>Edit Operator</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box box-warning">
                    <div class="panel-heading">
                        <h3>Edit Operator</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{ route('operator.update',[$operators->id]) }}" method="post"
                              class="validate"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put"/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="exampleInputEmail1">Operator</label>
                                        <input class="form-control required" tabindex="1"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                               required="" name="name" value="{{$operators->name}}">
                                        <span class="glyphicon  form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="exampleInputEmail1">Contact No.</label>
                                        <input class="form-control required" tabindex="1"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                               required="" name="contact_no" value="{{$operators->contact_no}}">
                                        <span class="glyphicon  form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                        <label for="exampleInputEmail1">Address</label>
                                        <input class="form-control required" tabindex="1"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                               required="" name="address" value="{{$operators->address}}">
                                        <span class="glyphicon  form-control-feedback"></span>
                                    </div>
                                </div>
                            </div>
                    <div class="box-footer">
                        <button tabindex="10" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{csrf_field()}}
                    </form>
                </div>
            </div>

        </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <!-- /.content -->
@endsection
