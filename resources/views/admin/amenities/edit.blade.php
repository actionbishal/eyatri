@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Edit Amenities
            <small>Edit Amenities</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box box-warning">
                    <div class="panel-heading">
                        <h3>Edit Amenities</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{ route('amenities.update',[$amenities->id]) }}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put"/>

                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <label for="exampleInputEmail1">Amenities</label>
                                    <input  class="form-control required" tabindex="1"
                                            data-parsley-trigger="change" data-parsley-minlength="2"
                                            data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                            required="" name="name" value="{{$amenities->name}}" >
                                    <span class="glyphicon  form-control-feedback"></span>
                                </div>
                                <div class="box-footer">
                                    <button tabindex="10" type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
    <!-- /.content -->
@endsection
