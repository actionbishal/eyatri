@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Gallery Management
            <small>Add new bus</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gallery management</li>
            <li class="active">Add new image</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="panel box-warning box">

                    <div class="panel-body">
                        <div class="alert alert-info">Add new Image</div>
                        <form role="form" action="{{route('gallery.store')}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <div class="box-body">
                                {{--<div class="col-md-6">--}}
                                <div class="row">
                                <div class="form-group col-md-6 ">
                                    <label>Bus </label>
                                    <select tabindex="-1"
                                            class="form-control select2 required select2-hidden-accessible"
                                            style="width: 100%;" name="bus_id" aria-hidden="true">
                                        <option></option>
                                        @foreach($buses as $bus)
                                            <option value="{{$bus->id}}">{{$bus->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Bus Image</label>
                                        <input type="file" class="form-control required" tabindex="1"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                               required="" name="image" >
                                        <span class="glyphicon  form-control-feedback"></span>
                                    </div>

                                </div>


                            </div>
                            <div class="col-md-12">
                                <div class="box-footer" style="text-align: left">
                                    <button tabindex="10" type="submit" class="btn btn-default">Save</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">View Image Details</h3>
                    </div>
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable"
                                           role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending"
                                                style="width: 182px;">Bus Name
                                            </th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending"
                                                style="width: 182px;">Images
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending"
                                                style="width: 224px;">Total Images
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending"
                                                style="width: 199px;">Actions
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($galleries as $gallery)
                                            <tr role="row" class="odd">
                                                <td>
                                                    sajha
                                                </td>
                                                <td class="sorting_1"><img src="{{ asset($gallery->image) }}" /></td>
                                                <td>
                                                   1
                                                </td>
                                                <td class="center">
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{route('gallery.edit', $gallery->id)}}">
                                                        <i class="fa fa-fw fa-edit"></i>Edit</a>

                                                                                                       <a class="btn btn-sm btn-danger"
                                                       href="{{route('gallery.destroy',$gallery->id)}}"
                                                       onclick="destroy(event,this)">
                                                        Delete
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Bus  Name</th>
                                            <th rowspan="1" colspan="1">Images</th>
                                            <th rowspan="1" colspan="1">Total Images</th>
                                            <th rowspan="1" colspan="1">Actions</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>
    <!-- /.content -->
@endsection
