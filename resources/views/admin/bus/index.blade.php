@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            View Bus Management Details
            <small>View Bus Management Details</small>
        </h1>
    </section>
    <!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="tab-content">
                    <div class="row panel-heading">
                        <div class="col-sm-2">
                            <a href="{{route('bus.create')}}" class="btn btn-default">+ Add Bus</a>
                        </div>
                    </div>
                    <div id="home" class="tab-pane fade in active">
                        <div class="box-body">
                            <table id="example1"
                                   class="table table-bordered table-striped table-responsive dataTable"
                                   role="grid" aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th class="hidden sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="ID: activate to sort column ascending" style="width: 0px;">
                                        ID
                                    </th>
                                    @if(Auth::user()->hasRole('Admin'))
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="route: activate to sort column ascending"
                                        style="width: 115px;">Operator Name
                                    </th>
                                    @endif
                                    <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Name: activate to sort column ascending"
                                        style="width: 74px;" aria-sort="descending">Bus Name
                                    </th>
                                    <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Name: activate to sort column ascending"
                                        style="width: 74px;" aria-sort="descending">Contact No.
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus RegiNumber: activate to sort column ascending"
                                        style="width: 122px;">Registration Number
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="route: activate to sort column ascending"
                                        style="width: 115px;">Route Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Type: activate to sort column ascending"
                                        style="width: 68px;">Bus Type
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Type: activate to sort column ascending"
                                        style="width: 68px;">Shift
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Type: activate to sort column ascending"
                                        style="width: 68px;">Amenities
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Type: activate to sort column ascending"
                                        style="width: 68px;">Approval
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0"
                                        rowspan="1" colspan="1"
                                        aria-label="Bus Type: activate to sort column ascending"
                                        style="width: 68px;">Status
                                    </th>
                                    <th width="200px;" class="sorting" tabindex="0"
                                        aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
                                        aria-label="Action: activate to sort column ascending"
                                        style="width: 199px;">Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($buses as $bus)
                                <tr role="row" class="table-row">
                                    <td class=" hidden center sorting_1">{{$bus->id}}</td>
                                    @if(Auth::user()->hasRole('Admin'))
                                    <td class="center">{{$bus->operator->name}}</td>
                                    @endif
                                    <td class="center">{{$bus->name}}</td>
                                    <td class="center">{{$bus->contact_no}}</td>
                                    <td class="center">{{$bus->registration_number}}</td>
                                    <td class="center">
                                        @if($bus->route)
                                        <a href="{{route('route.edit',$bus->route->id)}}">{{$bus->route->name}}
                                            <?php $locations = [] ?>
                                            @foreach($bus->route->cities  as $cities)
                                            <?php array_push($locations, $cities->name);?>
                                            @endforeach
                                            <p class="route-locations">({{implode(' - ', $locations)}})</p>
                                        </a>
                                        @else
                                        No Routes
                                        @endif

                                    </td>
                                    <td class="center">{{$bus->busType->name}}</td>
                                    <td class="center">{{$bus->shift}}</td>
                                <?php $a = [] ?>
                                    @foreach($bus->amenities  as $amenities)
                                    <?php array_push($a, $amenities->name);?>
                                    @endforeach
                                    <td class="center">{{implode(', ', $a)}}</td>
                                    @if(Auth::user()->hasRole('Admin'))
                                    <td>
                                        @if($bus->approval)
                                            <a class="btn btn-sm btn-success"
                                               href="{{route("bus.toggle-approval", $bus->id)}}">
                                                <i class="fa fa-fw fa-check"></i>Approved
                                            </a>
                                        @else
                                            <a class="btn btn-sm btn-danger"
                                               href="{{route("bus.toggleapproval", $bus->id)}}">
                                                <i class="fa fa-fw fa-spinner"></i>Pending
                                            </a>
                                        @endif
                                    </td>
                                        @else
                                        <td>
                                            @if($bus->approval)
                                                <i class="fa fa-fw fa-check"></i>Approved
                                            @else
                                                <i class="fa fa-fw fa-spinner"></i>Pending
                                            @endif
                                        </td>
                                    @endif
                                    <td>
                                        @if($bus->status)
                                            <a class="btn btn-sm btn-success"
                                               href="{{route("bus.toggle", $bus->id)}}">
                                                <i class="fa fa-fw fa-check"></i>Active
                                            </a>
                                        @else
                                            <a class="btn btn-sm btn-danger"
                                               href="{{route("bus.toggle", $bus->id)}}">
                                                <i class="fa fa-fw fa-spinner"></i>Inactive
                                            </a>
                                        @endif
                                    </td>

                                    <td class="center">
                                        <a class="btn btn-sm bg-olive" data-toggle="tooltip" data-placement="bottom" title="View" href="" data-toggle="modal"
                                           data-target="#myModal" onclick="setValue({{$bus}})">
                                            <i class="fa fa-fw fa-eye"></i></a>
                                        <a class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"
                                           href="{{route('bus.edit', $bus->id)}}">
                                            <i class="fa fa-fw fa-edit"></i></a>
                                        <a class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete"
                                           href="{{route('bus.destroy',$bus->id)}}"
                                           onclick="destroy(event,this)">
                                            <i class="fa fa-fw fa-times"></i>
                                        </a>
                                        <a class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Duplicate"
                                           href="#">
                                            <i class="fa fa-fw fa-caret-down"></i>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th class="hidden" rowspan="1" colspan="1">ID</th>
                                    @if(Auth::user()->hasRole('Admin'))
                                    <th rowspan="1" colspan="1">Operator</th>
                                    @endif
                                    <th rowspan="1" colspan="1">Bus Name</th>
                                    <th rowspan="1" colspan="1">Contact no.</th>
                                    <th rowspan="1" colspan="1">Bus RegiNumber</th>
                                    <th rowspan="1" colspan="1">Route Name</th>
                                    <th rowspan="1" colspan="1">Bus Type</th>
                                    <th rowspan="1" colspan="1">Shift</th>
                                    <th rowspan="1" colspan="1">Amenities</th>
                                    <th rowspan="1" colspan="1">Approval</th>
                                    <th rowspan="1" colspan="1">Status</th>
                                    <th rowspan="1" colspan="1">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;
                                            </button>
                                            <h4 class="modal-title">View Bus Management Details</h4>
                                        </div>
                                        <div class="modal-body">
                                            <b></b><b>Bus Name</b>
                                            <p class="bus_name"></p>
                                            <b></b><b>Bus RegiNumber</b>
                                            <p class="registration_number"></p>
                                            <b></b><b>Bus Type</b>
                                            <p class="type_id"></p>
                                            <b></b><b>Amenities</b>
                                            <p class="amenities"></p>
                                            <b></b><b>Route</b>
                                            <p class="route"></p>
                                            <b></b><b>Operator</b>
                                            <p class="route"></p>
                                            <b></b><b>Maximum Seats</b>
                                            <p class="maximum_seats"></p>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <script>
        function setValue(bus) {
            $(".bus_name").html(bus.name);
            $(".registration_number").html(bus.registration_number);
            $(".type_id").html(bus.type_id);
            let amenities = [];
            bus.amenities.forEach(function (value) {
                amenities.push(value.name);
            });
            $(".amenities").html(amenities.join(", "));
            $(".maximum_seats").html(bus.maximum_seats);
        }

        $(function () {
            $("#example1").DataTable();
        });

        function destroy(e, $this) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this  file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            _token: "{{ csrf_token() }}",
                            _method: 'delete'
                        },
                        url: $this.href,
                        statusCode: {
                            200: function (response) {
                                $this.closest("tr").remove();

                                swal("Poof! Your  file has been deleted!", "success");
                            },
                        }
                    });
                } else {
                    swal("Your file is safe!");
                }
            });

        }
    </script>
    <!-- /.content -->
@endsection
