<ul class="nav nav-pills">
    <li class="{{ Request::path() == 'admin/bus' ? 'active' : '' }}"><a  href="{{route('bus.index')}}">Buses</a></li>
    <li class="{{ Request::is('admin/bus-type*') ? 'active' : '' }}"><a href="{{route('bus-type.index')}}">Bus Type</a></li>
    <li class="{{ Request::is('admin/amenities*') ? 'active' : '' }}"><a href="{{route('amenities.index')}}">Amenities</a></li>
</ul>
