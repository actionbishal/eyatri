<div class="row stop-list loader-outer" style="margin-top: 10px;">
    <div class="loader" style="display: none"></div>
    <div class="col-sm-12">
        @foreach($route->cities as $index => $city)
            <div class="box">
                <div class="box-body">
                    <label>Location {{$index + 1}}: {{$city->name}}:</label>
                    <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>Stops Name</th>
                            <th>Description (optional)</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--loop for stops--}}
                        @foreach($bus->stops as $stop)
                            @if($city->id == $stop->city_id)
                                <tr>
                                    <td>
                                        <input class="form-control" type="text"
                                               name="stop_name[]"
                                               placeholder="Eg: Kalanki Bus Park"
                                               value="{{$stop->name}}">
                                    </td>
                                    <td>
                                        <input class="form-control" type="text"
                                               placeholder="More information"
                                               name="description[]"
                                               value="{{$stop->description}}">
                                    </td>
                                    <td>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text"
                                                   class="form-control input-small timepicker1"
                                                   name="time[]"
                                                   value="{{$stop->time}}">
                                            <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);"
                                           class="btn btn-danger"
                                           onclick="destroy(event,$(this))"
                                           data-id="{{$stop->id}}">
                                            <i class="fa fa-times"></i> </a>
                                        <input type="hidden" name="city[]"
                                               value="{{$city->id}}">
                                        <input type="hidden" name="stop[]"
                                               value="{{$stop->id}}">
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td>
                                <a class="btn btn-default add-stop-btn"
                                   data-city="{{$city->id}}">
                                    Add Stops +
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    </div>
</div>
