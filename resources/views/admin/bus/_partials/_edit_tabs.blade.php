<ul class="nav nav-pills">
    <li class="{{ Request::is('admin/bus/*/edit') ? 'active' : '' }}"><a  href="{{route('bus.index')}}">Bus Information</a></li>
    <li class="{{ Request::is('admin/bus/*/board_points') ? 'active' : '' }}"><a href="board-points">Boarding Points</a></li>
    <li class="{{ Request::is('admin/bus/*/drop_points') ? 'active' : '' }}"><a href="drop-points">Drop Points</a></li>
</ul>
