@extends('admin.layout.master')
@section('content')
    <section class="content-header">
        <h1>
            Bus Management
            <small>Add new bus</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bus management</li>
            <li class="active">Add new bus</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <!-- left column -->
                    <div class="panel-body">
                        <form role="form" action="{{route('bus.store')}}" method="post" class="validate"
                              enctype="multipart/form-data">
                            <div class="tab-content">
                                <div class="panel-body tab-pane  active" id="bus_info">
                                    <div class="alert alert-info"><i class="fa fa-info-circle"></i> Add new Bus
                                        <p>
                                            Use the form below to start creating your bus. You need to define a route
                                            that this
                                            bus
                                            operates on and a bus type. Then you'll have to define departure and arrival
                                            time
                                            for
                                            each location (bus stop) along the selected route. After saving you will be
                                            able to
                                            define the rest of the bus settings.
                                        </p>
                                    </div>
                                    <div class="row">
                                        @if(Auth::user()->hasRole('Admin'))
                                            <div class="form-group col-md-6 ">
                                                <label>Operator</label>
                                                <select tabindex="-1"
                                                        class="form-control select2 route required select2-hidden-accessible"
                                                        style="width: 100%;" name="operator_id" aria-hidden="true">
                                                    <option></option>
                                                    @foreach($operators as $operator)
                                                        <option value="{{$operator->id}}">{{$operator->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif
                                        <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="exampleInputEmail1">Bus Name</label>
                                            <input type="text" class="form-control required" tabindex="1"
                                                   data-parsley-trigger="change" data-parsley-minlength="2"
                                                   data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                                   name="name" placeholder="Bus Name">
                                            @if ($errors->has('name'))
                                                <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <label>Bus Type</label>
                                            <select tabindex="-1"
                                                    class="form-control select2 required select2-hidden-accessible bus_type "
                                                    style="width: 100%;" name="type_id" aria-hidden="true">
                                                <option></option>
                                                @foreach($bus_types as $bus_type)
                                                    <option value="{{$bus_type->id}}">{{$bus_type->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('type_id'))
                                                <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('type_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <label>Shift</label>
                                            <select tabindex="-1"
                                                    class="form-control select2 required select2-hidden-accessible bus_type "
                                                    style="width: 100%;" name="shift" aria-hidden="true">
                                                <option></option>
                                                @foreach($shifts as $shift)
                                                    <option >{{$shift}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('shift'))
                                                <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('shift') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <label>Contact No.</label>
                                            <input tabindex="2" type="text" class="form-control required"
                                                   name="contact_no"
                                                   data-parsley-trigger="change" data-parsley-minlength="2"
                                                   data-parsley-maxlength="15" placeholder="Contact No.">
                                            @if ($errors->has('contact_no'))
                                                <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('contact_no') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 {{ $errors->has('registration_number') ? ' has-error' : '' }}">
                                            <label for="exampleInputEmail1">Bus RegiNumber</label>
                                            <input tabindex="2" type="text" class="form-control required"
                                                   name="registration_number"
                                                   data-parsley-trigger="change" data-parsley-minlength="2"
                                                   data-parsley-maxlength="15" placeholder="Bus RegiNumber">
                                            @if ($errors->has('registration_number'))
                                                <span class="help-block form-control-feedback ">
                                                <strong>{{ $errors->first('registration_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Amenities</label>
                                            <select tabindex="-1"
                                                    class="form-control select2 js-example-basic-multiple select2-hidden-accessible"
                                                    style="width: 100%;" name="amenities[]" multiple="" id="amenities"
                                                    aria-hidden="true">

                                                @foreach($amenities as $amenity)
                                                    <option value="{{$amenity->id}}">{{$amenity->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="glyphicon  form-control-feedback"></span>
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                </div>
                            </div>
                            <div class="box-body">
                            </div>
                            <div class="col-md-12">
                                <div class="box-footer">
                                    <button tabindex="10" type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "-- Select --"
            });
            $('.bus_type').on('change', function () {
                $.ajax(
                    {
                        type: "GET",
                        url: "{{route('bus_type')}}",
                        data: {id: $(this).val()},
                        success: function (response) {
                            let options = [];
                            $.each(response, function (key, value) {
                                options.push({
                                    text: value.name,
                                    id: value.id
                                });
                            });

                            $('#seat-arrangement').empty().select2({data: options});
                        },
                        error: function (status) {
                            // alert(status);
                        },
                        complete: function () {
                            $('.loader').hide();
                        }
                    }
                );
            });
        });
    </script>
    <!-- /.content -->
@endsection
