@extends('admin.layout.master')
@section('content-header')
    <section class="content-header">
        <h1>
            Edit Bus Details
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">bus management</li>
            <li class="active">Edit Bus Details</li>
        </ol>
    </section>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form role="form" action="{{route('bus.update',[$bus->id])}}" method="post" class="validate"
                      enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put"/>
                    <div class="panel">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#bus_info">Bus Information</a></li>
                            <li class=""><a data-toggle="tab" href="#bus_stops">Bus Stops</a></li>
                            <li class=""><a data-toggle="tab" href="#bus_seats">Bus Seat Arrangements</a></li>
                            <li class=""><a data-toggle="tab" href="#bus_schedule">Bus Schedules</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="panel-body tab-pane  active" id="bus_info">
                                <div class="alert alert-info"><i class="fa fa-info-circle"></i> Update Bus Informations
                                    <p>
                                        Use the form below to update your bus.
                                    </p>
                                </div>
                                <div class="row">
                                    @if(Auth::user()->hasRole('Admin'))
                                        <div class="form-group col-md-4">
                                            <label>Operator</label>
                                            <select tabindex="-1"
                                                    class="form-control select2 required select2-hidden-accessible"
                                                    style="width: 100%;" name="operator_id" aria-hidden="true"
                                                    id="bus_types">
                                                @foreach($operators as $operator)
                                                    <option value="{{$operator->id}}"
                                                            {{$operator->id == $bus->operator->id ? "selected":""}}>
                                                        {{$operator->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class=" form-group col-md-4 has-feedback">
                                        <label class="control-label text-left" for="exampleInputEmail1">Bus
                                            Name</label>

                                        <input type="text" class="form-control required" tabindex="1"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" data-parsley-pattern="^[a-zA-Z\  \/]+$"
                                               name="name" value="{{$bus->name}}">
                                    </div>
                                    <div class="form-group col-md-4 has-feedback">
                                        <label for="exampleInputEmail1">Registration Number</label>
                                        <input tabindex="2" type="text" class="form-control"
                                               name="registration_number"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" placeholder="Bus RegiNumber"
                                               value="{{$bus->registration_number}}">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label>Amenities</label>
                                        <select tabindex="-1"
                                                class="form-control select2 js-example-basic-multiple select2-hidden-accessible"
                                                style="width: 100%;" name="amenities[]" multiple="" id="amenities"
                                                aria-hidden="true">
                                            @foreach($amenities as $amenity)
                                                <option value="{{$amenity->id}}"
                                                        {{in_array($amenity->id,$bus_amenities) ? "selected":""}}>
                                                    {{$amenity->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4 has-feedback">
                                        <label for="exampleInputEmail1">Contact No.</label>
                                        <input tabindex="2" type="text" class="form-control"
                                               name="contact_no"
                                               data-parsley-trigger="change" data-parsley-minlength="2"
                                               data-parsley-maxlength="15" placeholder="Contact No."
                                               value="{{$bus->contact_no}}">
                                    </div>

                                </div>
                                {{csrf_field()}}
                            </div>
                            <!-- Bus seats-->
                            <div class="panel-body tab-pane " id="bus_seats">
                                <div class="alert alert-info py-4"><i class="fa fa-info-circle"></i> Update Bus
                                    Informations
                                    <p>
                                        Use the form below to update your seat arrangements.
                                    </p>
                                </div>
                                <div class="row justify-content-center">

                                    <div class="form-group col-md-3">
                                        <label>Bus Type</label>
                                        <select tabindex="-1"
                                                class="form-control select2 required select2-hidden-accessible bus_type"
                                                style="width: 100%;" name="type_id" aria-hidden="true"
                                                id="bus_types">'
                                            <option></option>
                                            @foreach($bus_types as $bus_type)
                                                <option value="{{$bus_type->id}}"
                                                        {{$bus_type->id == $bus->busType->id ? "selected":""}}>
                                                    {{$bus_type->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Seat Arrangement</label>
                                        <select tabindex="-1"
                                                class="form-control select2 required"
                                                style="width: 100%;" name="seat_arrangement_id" id="seat-arrangement"
                                        >
                                            <option></option>

                                            @foreach($seat_arrangements as $seat_arrangement)
                                                <option value="{{$seat_arrangement->id}}"
                                                        {{$bus->seat_arrangement->first() && $seat_arrangement->id == $bus->seat_arrangement->first()->id ? "selected":""}}>
                                                    {{$seat_arrangement->name}}
                                                </option>
                                            @endforeach


                                        </select>
                                    </div>

                                    <div id="seat_layout" class="col-sm-6">
                                        <div class="card card-default">
                                            <div class="card-body">
                                                @if($bus->seat_arrangement->first())
                                                    <div class="seat_layout pull-left">
                                                        <div class="cabin_seats pull-left">
                                                            <?php $i = 1 ?>
                                                            @foreach($bus->seat_arrangement->first()->seats as
                                                            $seat)
                                                                @if($seat->isCabin)
                                                                    <span class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>
                                                        <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>
                                                    </span>
                                                                    @if($i%3==0)<br> @endif
                                                                    <?php $i++ ?>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                        <div class="pull-right">
                                                            <img src="{{asset('admin/img/steering.png')}}"
                                                                 style="margin-bottom:10px;" height="40px"
                                                                 width="40px">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <hr style="color: lightblue">

                                                        @if($bus->seat_arrangement->first()->seat_type =="1X2")
                                                            <?php $i = 1 ?>
                                                            @foreach($bus->seat_arrangement->first()->seats as
                                                            $seat)
                                                                @if(!$seat->isCabin)
                                                                    <span tabindex="1" class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>
                                                    <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>
                                                    </span>
                                                                    @if($i%4==0)<br> @endif
                                                                    <?php $i++ ?>
                                                                @endif
                                                            @endforeach
                                                        @endif


                                                    <!-- for 2X2-->

                                                        @if($bus->seat_arrangement->first()->seat_type =="2X2")
                                                            <?php $i = 1 ?>
                                                            @foreach($bus->seat_arrangement->first()->seats as
                                                            $seat)
                                                                @if(!$seat->isCabin)
                                                                    <span tabindex="1" class="fa-stack fa-lg seat-item">
                                                    <i class="fa {{$seat->status ? 'img-seat':'img-seat-o'}} fa-stack-2x"></i>
                                                    <span class="fa-stack-1x seat_text">{{$seat->name}}</span>
                                                    <input type="hidden" class="seat_name" name="seat_name[]"
                                                           value="{{$seat->name}}"/>
                                                    <input type="hidden" class="seat_status"
                                                           name="seat_status[]" value="{{$seat->status}}"/>
                                                    <input type="hidden" class="isCabin" name="isCabin[]"
                                                           value="{{$seat->isCabin}}"/>

                                                <input type="hidden" name="seat_id[]" value="{{$seat->id}}"/>

                                                    </span>
                                                                    @if($i%5==0)<br> @endif
                                                                    <?php $i++ ?>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </div>
                            <!--Bus stopping points-->

                            <div class="panel-body tab-pane" id="bus_stops">
                                @if($bus->route)
                                    <div class="alert alert-info"><i class="fa fa-info-circle"></i>Add or Update Bus
                                        stops
                                        <p>
                                            Use the form below to create and update your bus stops.
                                        </p>
                                    </div>
                                <!--                        <div class="row {{ $errors->has('route_id') ? ' has-error' : '' }}">-->
                                    <!--                            <label class="col-sm-1">Route:</label>-->
                                    <!--                            <div class="col-sm-5">-->
                                    <!--                                <select tabindex="-1"-->
                                    <!--                                        class="form-control select2 route required select2-hidden-accessible"-->
                                    <!--                                        style="width: 100%;" name="route_id" aria-hidden="true">-->
                                    <!--                                    <option></option>-->
                                <!--                                    @foreach($routes as $route)-->
                                <!--                                    <option value="{{$route->id}}"-->
                                <!--                                            {{$route->id == $bus->route_id ? "selected":""}}>-->
                                <!--                                        {{$route->name}}-->
                                    <!--                                    </option>-->
                                    <!--                                    @endforeach-->
                                    <!--                                </select>-->
                                    <!--                            </div>-->
                                    <!--                        </div>-->
                                    <div class="row stop-list loader-outer" style="margin-top: 10px;">
                                        <div class="loader" style="display: none"></div>
                                        <div class="col-sm-12">
                                            @foreach($bus->route->cities as $index => $city)
                                                <div class="box">
                                                    <div class="box-body">
                                                        <label>Location {{$index + 1}}: {{$city->name}}:</label>
                                                        <table class="table table-striped table-responsive">
                                                            <thead>
                                                            <tr>
                                                                <th>Stops Name</th>
                                                                <th>Description (optional)</th>
                                                                <th>Time</th>
                                                                <th>Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {{--loop for stops--}}
                                                            @foreach($bus->stops as $stop)
                                                                @if($city->id == $stop->city_id)
                                                                    <tr>
                                                                        <td>
                                                                            <input class="form-control" type="text"
                                                                                   name="stop_name[]"
                                                                                   placeholder="Eg: Kalanki Bus Park"
                                                                                   value="{{$stop->name}}">
                                                                        </td>
                                                                        <td>
                                                                            <input class="form-control" type="text"
                                                                                   placeholder="More information"
                                                                                   name="description[]"
                                                                                   value="{{$stop->description}}">
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-group bootstrap-timepicker timepicker">
                                                                                <input type="text"
                                                                                       class="form-control input-small timepicker1"
                                                                                       name="time[]"
                                                                                       value="{{$stop->time}}">
                                                                                <span class="input-group-addon"><i
                                                                                            class="glyphicon glyphicon-time"></i></span>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0);"
                                                                               class="btn btn-danger"
                                                                               onclick="destroy(event,$(this))"
                                                                               data-id="{{$stop->id}}">
                                                                                <i class="fa fa-times"></i> </a>
                                                                            <input type="hidden" name="city[]"
                                                                                   value="{{$city->id}}">
                                                                            <input type="hidden" name="stop[]"
                                                                                   value="{{$stop->id}}">
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                            <tr>
                                                                <td>
                                                                    <a class="btn btn-default add-stop-btn"
                                                                       data-city="{{$city->id}}">
                                                                        Add Stops +
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @else
                                        <div class="card">
                                            <div class="card-body text-center">
                                                Please Create some Routes first
                                            </div>
                                        </div>
                                    @endif

                            </div>
                            {{--Schedule Arrangement--}}
                            <div class="panel-body tab-pane" id="bus_schedule">
                                <div class="alert alert-info"><i class="fa fa-info-circle"></i> Update Bus Schedule
                                    <p>
                                        Use the form below to update your bus Schedule.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Recuring:</h2><br>
                                        <input type="hidden" value="{{$bus->id}}" name="bus_id">
                                        <input type="checkbox" id="sunday" name="Sunday" value="1" checked="checked">
                                        <label>Every Sunday</label><br>
                                        <input type="checkbox" id="monday" name="Monday" value="1" checked="checked">
                                        <label>Every Monday</label><br>
                                        <input type="checkbox" id="tuesday" name="Tuesday" value="1" checked="checked">
                                        <label>Every Tuesday</label><br>
                                        <input type="checkbox" id="wednesday" name="Wednesday" value="1"
                                               checked="checked">
                                        <label>Every Wednesday</label><br>
                                        <input type="checkbox" id="thursday" name="Thursday" value="1"
                                               checked="checked">
                                        <label>Every Thursday</label><br>
                                        <input type="checkbox" id="Friday" name="Friday" value="1" checked="checked">
                                        <label>Every Friday</label><br>
                                        <input type="checkbox" id="Saturday" name="Saturday" value="1"
                                               checked="checked">
                                        <label>Every Saturday</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="tr_clone" style="display:none">
                        <tbody>
                        <tr>
                            <td>
                                <input type="hidden" name="stop[]">
                                <input class="form-control" type="text"
                                       placeholder="Eg: Kalanki Bus Park" name="stop_name[]">
                            </td>
                            <td>
                                <input class="form-control" type="text"
                                       placeholder="More information" name="description[]">
                            </td>
                            <td>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input type="text"
                                           class="form-control input-small timepicker1"
                                           name="time[]">
                                    <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-time"></i></span>
                                </div>
                            </td>
                            <td>
                                <a href="javascript:void(0);"
                                   class="btn btn-danger"
                                   onclick="destroy(event,$(this))">
                                    <i class="fa fa-times"></i>
                                </a>
                                <input type="hidden" name="city[]">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="box-footer col-md-12">
                        <button tabindex="10" type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('page-scripts')
    <script>

        $(document).ready(function () {
            $('.timepicker1').timepicker();
            $(".select2").select2({
                placeholder: "-- Select --",
            });
            let init_add_btn = function () {
                $(".add-stop-btn").on('click', function (e) {
                    e.preventDefault();
                    let tr_clone = $('#tr_clone').find('tbody');
                    let city_id = $(this).data('city');
                    tr_clone.find('td:nth-child(4) input:hidden').val(city_id);
                    let clone_text = tr_clone.html();
                    $(this).closest('tr').before(clone_text);
                    $('.timepicker1').timepicker();
                });
            };
            init_add_btn();

            $('.route').on('change', function () {
                $.ajax(
                    {
                        type: "GET",
                        url: "{{route('route-list')}}",
                        data: {id: $(this).val(), bus: "{{$bus->id}}"},
                        beforeSend: function () {
                            $('.loader').show();
                        },
                        success: function (response) {
                            $('.stop-list').replaceWith(response);
                            init_add_btn();
                            $('.timepicker1').timepicker();
                        },
                        error: function (status) {
                            // alert(status);
                        },
                        complete: function () {
                            $('.loader').hide();
                        }
                    }
                );
            });
            $('.bus_type').on('change', function () {
                $.ajax(
                    {
                        type: "GET",
                        url: "{{route('bus_type')}}",
                        data: {id: $(this).val()},
                        success: function (response) {
                            let options = [];
                            $.each(response, function (key, value) {
                                options.push({
                                    text: value.name,
                                    id: value.id
                                });
                            });

                            $('#seat-arrangement').empty().select2({data: options});
                        },
                        error: function (status) {
                            // alert(status);
                        },
                        complete: function () {
                            $('.loader').hide();
                        }
                    }
                );
            });
            $('#seat-arrangement').on('change', function () {
                let id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: '/admin/seat-arrangement/' + id,
                    success: function (response) {
                        $('#seat_layout').html(response);
                        // seat_modal.modal('show');
                    }
                });
            });

            function destroy(e, $this) {
                e.preventDefault();
                if ($this.data("id")) {
                    $.ajax(
                        {
                            type: "GET",
                            url: "{{route('stop-delete')}}",
                            data: {
                                id: $this.data("id")
                            },
                            success: function (response) {
                                $this.closest('tr').fadeOut('fast',
                                    function () {
                                        $(this).remove();
                                    });
                            }
                        }
                    )
                } else
                    $this.closest('tr').fadeOut('fast',
                        function () {
                            $(this).remove();
                        });
            }

        });
    </script>
@endsection

