@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{route('search.index')}}" method="get">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-2">
                    From:<input type="text" class="form-control" id="name" placeholder="Kathmandu" name="source">
                </div>
                <div class="col-sm-2">
                    To:<input type="text" class="form-control" id="name" placeholder="Pokhara" name="destination">
                </div>
                <div class="col-sm-3 ">
                    Date:<input class="form-control" type="date" name="date" value="2019-01-13">
                </div>
                <div class="col-sm-3">
                    Shift:</br>
                    Day
                    <input type="radio" name="shift" value="day" checked>
                    Night
                    <input type="radio" name="radio" name="shift" value="night">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-danger" type="submit"> Search</button>
                </div>
            </div>
            <hr style="height:1px" color="#3c8dbc">
        </form>
        @foreach($buses as $bus)
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title">{{$bus->name}} /
                                <small class="message">{{$bus->busType->name}}</small>
                            </h4>
                            <a href="#bus_stops_{{$bus->id}}" style="text-decoration: none" data-toggle="collapse">Bus
                                stops</a><span style="margin-left: 40px"><a href="#">Policies</a></span>
                        </div>
                        <div class="col-sm-2">
                            <h5 class="card-title">5 Ratings</h5>
                            @foreach($bus->amenities as $amenity)
                                <span class="message">{{$amenity->name}}</span>
                            @endforeach
                        </div>
                        <div class="col-sm-4 text-right">
                            <h4 class="">Rs 500</h4>
                            <a class="btn btn-outline-primary" href="#">Select Seats</a>
                        </div>
                    </div>
                    <div class="row collapse" id="bus_stops_{{$bus->id}}">
                        @foreach($bus->route->cities as $city)
                            <div class="col-sm-3">
                                <p>{{$city->name}}</p>
                                @foreach($bus->stops as $stop)
                                    @if($stop->city_id == $city->id)
                                        <input type="radio" name="stop">&nbsp;<b>{{$stop->time}}</b>
                                        &nbsp;{{$stop->name}}<br>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        @endforeach
    </div>
@endsection
