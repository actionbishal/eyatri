@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{route('search.index')}}" method="get">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-2">
                    From:<input type="text" class="form-control" id="source" placeholder="Kathmandu" value="Kathmandu" name="source">
                </div>
                <div class="col-sm-2">
                    To:<input type="text" class="form-control" id="destination"  placeholder="Pokhara" value="Pokhara" name="destination">
                </div>
                <div class="col-sm-3 ">
                    Date:<input class="form-control" type="date" name="date" value="2019-01-13">
                </div>
                <div class="col-sm-3">
                    Shift:</br>
                    Day
                    <input type="radio" name="shift" value="day" checked>
                    Night
                    <input type="radio" name="radio" name="shift" value="night">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-danger " type="submit"> Search</button>
                </div>
            </div>
            <hr style="height:1px" color="#3c8dbc">
        </form>
    </div>
@endsection
