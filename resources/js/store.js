import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
import axios_custom from './axios_custom'

Vue.use(Vuex);


export const store = new Vuex.Store({
    state: {
        isEmployer: localStorage.getItem('isEmployer')|| false,
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        Activated(state) {
            return state.activated;
        },
        isEmployer(state) {
            return state.isEmployer;
        },

    },
    mutations: {
        updateFilter(state, filter) {
            state.filter = filter
        },

        retrieveToken(state, token) {
            state.token = token
        },
        CheckActivation(state, value) {
            state.activated = value
        },
        destroyToken(state) {
            state.token = null
        }
    },
    actions: {

        getCompany(context) {
            axios_custom.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios_custom.get('/company')
                        .then(response => {
                            resolve(response)
                                // console.log(response);
                                // context.commit('addTodo', response.data)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        search(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('api/search', data)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        // document.getElementById("overlay").style.display = "none";
                        reject(error)
                    })
            })
        },
        getSeatArrangement(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('api/seat-arrangement', data)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        // document.getElementById("overlay").style.display = "none";
                        reject(error)
                    })
            })
        },
    }
})
