import SearchComponent from "./components/search/SearchBusComponent";
import HomeComponent from "./components/HomeComponent";
import BusTravellerComponent from "./components/BusTravellerComponent";

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeComponent
    },
    {
        path: '/search',
        name: 'search',
        component: SearchComponent
    },
    {
        path: '/bustraveller',
        name: 'bustraveller',
        component: BusTravellerComponent
    },
];

export default routes
