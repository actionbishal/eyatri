import axios from 'axios'

const axios_custom = axios.create({
  baseURL: 'http://eyatri.test/api'
})

// before a request is made start the nprogress
axios_custom.interceptors.request.use(config => {
  // NProgress.start();
  document.getElementById("overlay").style.display = "block";
  // $('.modal-content').each(function(i, obj) {
  //   this.classList.add("overlay_custom");
  //   });
  return config
});

// before a response is returned stop nprogress
axios_custom.interceptors.response.use(response => {
  // NProgress.done();
  document.getElementById("overlay").style.display = "none";
  // $('.modal-content').each(function(i, obj) {
  //   this.classList.remove("overlay_custom");
  //   });
  return response
})

export default axios_custom