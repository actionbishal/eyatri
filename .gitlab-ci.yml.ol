stages:
  - test
  - deploy
# Variables
variables:
  MYSQL_ROOT_PASSWORD: root
  MYSQL_USER: homestead
  MYSQL_PASSWORD: secret
  MYSQL_DATABASE: homestead
  DB_HOST: mysql

image: edbizarro/gitlab-ci-pipeline-php:7.1-alpine

test:
  stage: test
  services:
    - mysql:5.7
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress
    - cp .env.example .env
    - php artisan key:generate
    - php artisan migrate:refresh --seed
    - npm install --progress=false
    - npm run dev
  except:
    - develop

deploy_develop:
  stage: deploy
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh-add <(echo "$PRIVATE_KEY")
#    - "apt-key update && apt-get update && apt-get install -y -qq sshpass"
#    - export SSHPASS=$USER_PASS
#    - sshpass -e scp -P 2222 -o stricthostkeychecking=no -r . fhrvi86w@e-yatri.com:~/public_html/eyatri
##     - sshpass -e ssh -p 2222 -o stricthostkeychecking=no fhrvi86w@e-yatri.com "cd ~/eyatri && php -v &&  /opt/cpanel/composer/bin/composer install && chmod 755 -R . && chmod -R o+w storage && php artisan migrate:refresh --seed && npm install --progress=false && npm run prod"
    - ssh -p 2222 -o stricthostkeychecking=no fhrvi86w@e-yatri.com "-y && cd ~/public_html/eyatri && git pull origin develop && php -v && php -d allow_url_fopen=1 -d detect_unicode=0 -d suhosin.executor.include.whitelist=phar &&  /opt/cpanel/composer/bin/composer install && chmod 755 -R . && chmod -R o+w storage && php artisan migrate && php artisan db:seed && npm install --progress=false && npm run prod"

  environment:
    name: develop
    url: https://e-yatri.com
  only:
     - develop
  when: on_success

